import numpy as np
import matplotlib.pyplot as plt
import csv

def load_csv(file_path, file_name ):
    with open(file_path+file_name, 'r' ) as csv_file:
        each_csv = np.loadtxt(csv_file ,delimiter=',')
    return each_csv

""" | central value | total systematics | fitting method | BinWidth | FitRange | """
def CalSys(dict_sys_list ):
    
    xticks = []
    xaxis_indice=[]
    alpha_nom=[]
    error_sys=[]
    error_fit=[]
    error_wid=[]
    error_ran=[]
    total_Unc=[]
    frac_sys=[]
    frac_fit=[]
    frac_wid=[]
    frac_ran=[]
    xaxis_indice=[]

    index_i = 0
    for key in dict_sys_list:
        xaxis_indice.append(index_i)
        xticks.append(key)
        alpha_nom.append(dict_sys_list[key][0])
        error_sys.append(dict_sys_list[key][1])
        error_fit.append(dict_sys_list[key][2])
        #error_wid.append(dict_sys_list[key][3])
        #error_ran.append(dict_sys_list[key][4])
        total_err = np.sqrt( dict_sys_list[key][1]**2 + dict_sys_list[key][2]**2 ) #+ dict_sys_list[key][3]**2 + dict_sys_list[key][4]**2 )
        total_Unc.append(total_err)
        #frac_sys.append((dict_sys_list[key][1]*dict_sys_list[key][1])/(total_err))
        #frac_fit.append((dict_sys_list[key][2]*dict_sys_list[key][2])/(total_err))
        #frac_wid.append((dict_sys_list[key][3]*dict_sys_list[key][3])/(total_err))
        #frac_ran.append((dict_sys_list[key][4]*dict_sys_list[key][4])/(total_err))
        index_i += 1

    print(xaxis_indice)
    print(xticks)
    print("alpha",alpha_nom)
    print("sys"  ,error_sys)
    print("fit"  ,error_fit)
    #print("width",error_wid)
    #print("range",error_ran)

    print("total",total_Unc)
    #print(frac_sys)
    #print(frac_fit)
    #print(frac_wid)
    #print(frac_ran)

    
    y_up=2
    y_dw=-4.5

    fig, ax = plt.subplots(1,1)
    
    ax.set_xticks(range(len(xaxis_indice)))
    ax.set_xticklabels(xticks, fontsize=6)
    ax.set_yticks([2,1.5,1,0.5,0,-0.5,-1,-1.5,-2,-2.5,-3,-3.5,-4,-4.5,-5])
    ax.set_ylim( [-3,2] )
    ax.set_ylabel(r"$\alpha$[%]" , fontsize=10)
    plt.setp(ax.get_xticklabels(), rotation=-10, fontsize=8, ha="left", rotation_mode="anchor")
    
    plt.axhline(y=0, color='grey', linestyle='-', linewidth = 0.5)
    
    plt.errorbar(xaxis_indice,                  alpha_nom, yerr=total_Unc,   fmt='none', ecolor='grey' , fillstyle='full' , capsize=2.5, elinewidth=5, alpha=0.7, label='Total Uncertainty')
    plt.errorbar([x+0.1 for x in xaxis_indice], alpha_nom, yerr=error_sys,   fmt='none', ecolor='#649CD9' , fillstyle='full' , capsize=2.5, elinewidth=5, alpha=0.7, label='Absolute syst error')
    plt.errorbar([x+0.2 for x in xaxis_indice], alpha_nom, yerr=error_fit,   fmt='none', ecolor='#FFED9E' , fillstyle='full' , capsize=2.5, elinewidth=5, alpha=0.7, label='Absolute fitting error')
    #plt.errorbar([x+0.3 for x in xaxis_indice], alpha_nom, yerr=error_wid,   fmt='none', ecolor='#F2AC42' , fillstyle='full' , capsize=2.5, elinewidth=5, alpha=0.7, label='Absolute width error')
    #plt.errorbar([x+0.4 for x in xaxis_indice], alpha_nom, yerr=error_ran,   fmt='none', ecolor='#E16071' , fillstyle='full' , capsize=2.5, elinewidth=5, alpha=0.7, label='Absolute range error')
    plt.errorbar(xaxis_indice, alpha_nom, fmt='.k'  , ecolor='g' , capsize=5, label='Nominal')
    
    #xaxis_indice=np.array(xaxis_indice)
    #alpha_nom = np.array(alpha_nom)
    #error_total = np.array(error_total)
    #error_fit = np.array(error_fit)
    #error_sys = np.array(error_sys)

    #plt.fill_between(xaxis_indice, alpha_nom-error_total, alpha_nom+error_total, linewidth = 6,  color='b'    ,  label='Total Uncertainty')
    #plt.fill_between(xaxis_indice, alpha_nom-error_fit,   alpha_nom+error_fit,   linewidth = 4,  color='g'    ,  label='From fit')
    #plt.fill_between(xaxis_indice, alpha_nom-error_sys,   alpha_nom+error_sys,   linewidth = 2,  color='grey' ,  label='From Syst')
    #plt.fill_between(xaxis_indice, alpha_nom, fmt='.k'  , ecolor='g' , capsize=5, label='Nominal')
    

    plt.axvline(2.75 , linestyle='--', color='grey', linewidth = 0.5)
    #plt.axvline(5.5 , linestyle='-', color='grey', linewidth = 0.5)
    #plt.axvline(8.5 , linestyle='-', color='grey', linewidth = 0.5)
    #plt.axvline(11.5, linestyle='-', color='grey', linewidth = 0.5)
    #plt.axvline(14.5, linestyle='-', color='grey', linewidth = 0.5)

    #plt.text(0  , y_dw + 0.05 , s="1p;All"     , fontsize=8 )
    #plt.text(3  , y_dw + 0.05 , s="1p;Forward" , fontsize=8 )
    #plt.text(6  , y_dw + 0.05 , s="1p;Central" , fontsize=8 )
    #plt.text(9  , y_dw + 0.05 , s="3p;All"     , fontsize=8 )
    #plt.text(12 , y_dw + 0.05 , s="3p;Forward" , fontsize=8 )
    #plt.text(15 , y_dw + 0.05 , s="3p;Central" , fontsize=8 )

    plt.grid(color = 'grey', linestyle = '--', linewidth = 0.2, axis = 'y')
    #plt.grid(color = 'green', linestyle = '--', linewidth = 0.5, axis = 'x')
    plt.legend(loc='upper left', fontsize = 8)

    fig.tight_layout()
    fig.savefig("Bkg_221_Sig_221.png", dpi=600)
    

    #xticks = []
    #delta_alpha = []
    #delta_alpha_err = []
    #sorted_Sys_keys = sorted( Sys_DeltaAlpha_Map, key=lambda dict_key: abs(Sys_DeltaAlpha_Map[dict_key]) , reverse=True )
    ##print(sorted_Sys_keys)
    #
    #for key in sorted_Sys_keys:
    #    xticks.append(key)
    #    delta_alpha.append(Sys_DeltaAlpha_Map[key]*Sys_Alpha_Map[NominalName])
    #    delta_alpha_err.append(abs(Sys_DeltaAlpha_Map[key]))

    #fig, ax = plt.subplots(1,1)
    #ax.bar(range(len(xticks)), delta_alpha, align='center')
    #ax.set_xticks(range(len(xticks)))
    #ax.set_xticklabels(xticks, fontsize=6)
    #plt.setp(ax.get_xticklabels(), rotation=30, ha="right", rotation_mode="anchor")

    #ax.text(0.59, 0.93, r"$\alpha_{Nom \pm Sys}=$"+str(round(Sys_Alpha_Map[NominalName],2))+"%" + 
    #        r"$\pm$"+str( abs(round(Sys_Alpha_Map[NominalName]*alphaSys_value,2)) )+"%" , fontsize=12, transform = ax.transAxes)
    #ax.text(0.59, 0.86, file_head[:-1] , fontsize=10, transform = ax.transAxes)
    #ax.text(0.59, 0.79, r"total relative $\sigma_{\alpha}=$"+str(round(100*alphaSys_value,1))+"%" , fontsize=10, transform = ax.transAxes)
    #ax.text(0.59, 0.72, "rel: xx%", fontsize=10 , color = 'g', transform = ax.transAxes)
    #
    #ax.text(0.03, 0.92, r'ATLAS',fontsize=16, weight="bold",style="italic", transform = ax.transAxes)
    #ax.text(0.19, 0.92, "Internal" , fontsize=16 , transform = ax.transAxes)
    #
    #ax.plot([-0.5,len(delta_alpha)-0.5],[0,0],linewidth=1,color='0.8',linestyle=(0,(1,1)) ) 
    #for i in range(len(delta_alpha)):
    #    x_pos = i - 0.3
    #    if delta_alpha[i] != 0:
    #        y_pos = delta_alpha[i] + 0.06 * delta_alpha[i]/abs(delta_alpha[i]) - 0.03
    #    else:
    #        y_pos = delta_alpha[i] + 0.06 - 0.03
    #    ax.text(x_pos, y_pos, str(round(100*delta_alpha_err[i],1))+"%", color='g',fontsize=6)

    #ax.set_ylim( [-1,1] )
    #ax.set_ylabel(r"$\Delta\alpha$[%]" , fontsize=14)
    #ax.set_xlabel("Systematic" , fontsize=14)
    #
    #fig.tight_layout()
    #fig.savefig(file_head+".png", dpi=600)
    #fig.savefig(file_head+".eps")


#CalSys("./data.csv")
#CalSys("./data_pT.csv")
in_path = "./"

in_dict={}
in_dict["prong_1_AllRegion"    ] = load_csv(in_path,"prong_1_AllRegion.csv") 
in_dict["prong_1_CentralRegion"] = load_csv(in_path,"prong_1_CentralRegion.csv") 
in_dict["prong_1_ForwardRegion"] = load_csv(in_path,"prong_1_ForwardRegion.csv") 
in_dict["prong_3_AllRegion"    ] = load_csv(in_path,"prong_3_AllRegion.csv") 
in_dict["prong_3_CentralRegion"] = load_csv(in_path,"prong_3_CentralRegion.csv") 
in_dict["prong_3_ForwardRegion"] = load_csv(in_path,"prong_3_ForwardRegion.csv") 
print(in_dict)
CalSys(in_dict)
