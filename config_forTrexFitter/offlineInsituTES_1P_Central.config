Job: "Data"
  ReadFrom: HIST
  HistoPath:"."
  Label: "#tau ID in Z #rightarrow #mu#tau"
  CmeLabel: "13 TeV"
  POI: "TES_correction"
  LumiLabel: "139 fb^{-1}"
  DebugLevel: 1
  AtlasLabel: "Internal"
  ImageFormat: pdf,png
  HistoChecks: NOCRASH
  SystPruningShape: 0.01
  SystPruningNorm: 0.01
  SmoothingOption: TTBARRESONANCE
  RankingMaxNP: 20
  RankingPlot: Merge
  CorrelationThreshold: 0.1
  UseGammasForCorr: True
  RankingPOIName: "#alpha_{TES}"
  SignalRegionsPlot:"SR_ztt", "ENDL"
  GetChi2: STAT+SYST
  PlotOptions: CHI2
  DoSummaryPlot: FALSE
  DoPieChartPlot: FALSE
  SystDataPlots: TRUE
  OutputDir:"prong_1_CentralRegion"
  SystControlPlots: TRUE


Fit: "insituTESFit"
  FitType: SPLUSB
  FitRegion: CRSR
  FitBlind: FALSE
  POIAsimov: TES_correction@0.0
  UseMinos: "all"
  doLHscan: all
  SaturatedModel: TRUE
  GetGoodnessOfFit: TRUE
  TemplateInterpolationOption: SQUAREROOT


### Fit Regions ###

Region: "SR_ztt"
  Type: SIGNAL
  VariableTitle: "M(#mu, #tau)"
  Label: "Z #rightarrow #mu+#tau SR"


#### Samples ####

## GHOST ##
Sample: "Nom_Ztt"
  Type: GHOST
  Title: "Ztt"
  HistoFile: "Ztt_PoPy8_0p0000"
  HistoName: "prong_1_CentralRegion/Nominal"
  Regions: SR_ztt


### Signal ###

## Negative ##



Sample: "Signal_m0p40"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_m0p0040"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",-0.40
  Regions: SR_ztt

Sample: "Signal_m0p30"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_m0p0030"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",-0.30
  Regions: SR_ztt

Sample: "Signal_m0p20"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_m0p0020"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",-0.20
  Regions: SR_ztt

Sample: "Signal_m0p10"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_m0p0010"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",-0.10
  Regions: SR_ztt

Sample: "Signal_m0p05"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_m0p0005"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",-0.05
  Regions: SR_ztt

## Zero ##

Sample: "Signal_0p00"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0000"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",0.0
  Regions: SR_ztt

## Positive ##

Sample: "Signal_0p05"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0005"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",0.05
  Regions: SR_ztt

Sample: "Signal_0p10"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0010"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",0.10
  Regions: SR_ztt

Sample: "Signal_0p20"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0020"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",0.20
  Regions: SR_ztt

Sample: "Signal_0p30"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0030"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",0.30
  Regions: SR_ztt

Sample: "Signal_0p40"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0040"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",0.40
  Regions: SR_ztt

Sample: "Signal_0p50"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0050"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",0.50
  Regions: SR_ztt

Sample: "Signal_0p60"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0060"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",0.60
  Regions: SR_ztt

Sample: "Signal_0p70"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0070"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",0.70
  Regions: SR_ztt

Sample: "Signal_0p80"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0080"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",0.80
  Regions: SR_ztt

Sample: "Signal_0p90"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0090"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",0.90
  Regions: SR_ztt

Sample: "Signal_1p00"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0100"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",1.00
  Regions: SR_ztt

Sample: "Signal_1p10"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0110"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",1.10
  Regions: SR_ztt

Sample: "Signal_1p20"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0120"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",1.20
  Regions: SR_ztt

Sample: "Signal_1p30"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0130"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",1.30
  Regions: SR_ztt

Sample: "Signal_1p40"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0140"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",1.40
  Regions: SR_ztt

Sample: "Signal_1p50"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0150"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",1.50
  Regions: SR_ztt

Sample: "Signal_1p60"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0160"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",1.60
  Regions: SR_ztt

Sample: "Signal_1p80"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0180"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",1.80
  Regions: SR_ztt

Sample: "Signal_2p00"
  Type: SIGNAL
  Title: "Ztt"
  FillColor: 861
  LineColor: 861
  HistoFile: "Ztt_PoPy8_0p0200"
  HistoName: "prong_1_CentralRegion/Nominal"
  Morphing: "TES_correction",2.00
  Regions: SR_ztt

# data #
Sample: "Data"
  Title: "Data"
  Type: DATA
  HistoFile: "Data_0p0"
  HistoName: "prong_1_CentralRegion/NOMINAL"
  Regions: SR_ztt

# backgrounds #
Sample: "Wl_fake"
  Type: BACKGROUND
  Title: "Fake Wl"
  FillColor: 881
  LineColor: 881
  HistoFile: "Wl_PoPy8_0p0000"
  HistoName: "prong_1_CentralRegion/Nominal"
  Regions: SR_ztt

Sample: "Wt_fake"
  Type: BACKGROUND
  Title: "Fake Wt"
  FillColor: 400
  LineColor: 400
  HistoFile: "Wt_PoPy8_0p0000"
  HistoName: "prong_1_CentralRegion/Nominal"
  Regions: SR_ztt

Sample: "Zll_fake"
  Type: BACKGROUND
  Title: "Fake Z #rightarrow ll"
  FillColor: 422
  LineColor: 422
  HistoFile: "Zll_PoPy8_0p0000"
  HistoName: "prong_1_CentralRegion/Nominal"
  Regions: SR_ztt

Sample: "TTbar_fake"
  Title: "Fake TTbar"
  Type: BACKGROUND
  FillColor: 72
  LineColor: 72
  HistoFile: "TTbar_0p0000"
  HistoName: "prong_1_CentralRegion/Nominal"
  Regions: SR_ztt

Sample: "STbar_fake"
  Title: "Fake SingleTop"
  Type: BACKGROUND
  FillColor: 819
  LineColor: 819
  HistoFile: "SingleTop_0p0000"
  HistoName: "prong_1_CentralRegion/Nominal"
  Regions: SR_ztt

Sample: "QCD_fake"
  Title: "Fake Rest"
  Type: BACKGROUND
  FillColor: 42
  LineColor: 42
  HistoFile: "QCD_Data_0p0"
  HistoName: "prong_1_CentralRegion/Nominal"
  Regions: SR_ztt


#### Normfactors ####

NormFactor: "nf_ztt"
  Title: "NF Ztt"
  Nominal: 1
  Min: 0.5
  Max: 1.5
  Samples: Signal_*p*
  Regions: SR_ztt

NormFactor: "TES_correction"
  Title: "TES_correction"
  Nominal: 0.0
  Min: -1.9
  Max: 1.9
  Samples: none

#### Weight Systematics ####

### SF or TF ###

Systematic: "W_SF"
  Samples: Wl_fake, Wt_fake, QCD_fake
  Regions: SR_ztt
  Title: "W_SF"
  Type: HISTO
  HistoNameUp: "prong_1_CentralRegion/W_SF_UP"
  HistoNameDown: "prong_1_CentralRegion/W_SF_DOWN"
  Category: SFnTF

Systematic: "QCD_TF"
  Samples: QCD_fake
  Regions: SR_ztt
  Title: "QCD_TF"
  Type: HISTO
  HistoNameUp: "prong_1_CentralRegion/QCD_TF_UP"
  HistoNameDown: "prong_1_CentralRegion/QCD_TF_DOWN"
  Category: SFnTF


### Lumi ###

Systematic: "Lumi"
  Title: "Lumi"
  NuisanceParameter: "Lumi"
  Type: HISTO
  ReferenceSample: Nom_Ztt
  ReferencePruning: Signal_0p00
  Samples: Signal_*p*
  HistoFileUp: "Ztt_PoPy8_0p0000"
  HistoFileDown: "Ztt_PoPy8_0p0000"
  HistoNameUp: "prong_1_CentralRegion/lumi_UP"
  HistoNameDown: "prong_1_CentralRegion/lumi_DOWN"
  Symmetrisation: TwoSided
  Category: Lumi
  Regions: SR_ztt

Systematic: "Lumi"
  Title: "Lumi"
  NuisanceParameter: "Lumi"
  Type: HISTO
  Samples: Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
  HistoNameUp: "prong_1_CentralRegion/lumi_UP"
  HistoNameDown: "prong_1_CentralRegion/lumi_DOWN"
  Symmetrisation: TwoSided
  Category: Lumi
  Regions: SR_ztt

### Pu ###

Systematic: "Pileup"
  Title: "Pileup"
  NuisanceParameter: "Pileup"
  Type: HISTO
  ReferenceSample: Nom_Ztt
  ReferencePruning: Signal_0p00
  Samples: Signal_*p*
  HistoFileUp: "Ztt_PoPy8_0p0000"
  HistoFileDown: "Ztt_PoPy8_0p0000"
  HistoNameUp: "prong_1_CentralRegion/pileup_UP"
  HistoNameDown: "prong_1_CentralRegion/pileup_DOWN"
  Symmetrisation: TwoSided
  Category: Pileup
  Regions: SR_ztt

Systematic: "Pileup"
  Title: "Pileup"
  NuisanceParameter: "Pileup"
  Type: HISTO
  Samples: Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
  HistoNameUp: "prong_1_CentralRegion/pileup_UP"
  HistoNameDown: "prong_1_CentralRegion/pileup_DOWN"
  Symmetrisation: TwoSided
  Category: Pileup
  Regions: SR_ztt

### Trigger ###

Systematic: "TrigStat"
  Title: "TrigStat"
  NuisanceParameter: "TrigStat"
  Type: HISTO
  ReferenceSample: Nom_Ztt
  ReferencePruning: Signal_0p00
  Samples: Signal_*p*
  HistoFileUp: "Ztt_PoPy8_0p0000"
  HistoFileDown: "Ztt_PoPy8_0p0000"
  HistoNameUp: "prong_1_CentralRegion/TrigStat_UP"
  HistoNameDown: "prong_1_CentralRegion/TrigStat_DOWN"
  Symmetrisation: TwoSided
  Category: Trigger
  Regions: SR_ztt

Systematic: "TrigStat"
  Title: "TrigStat"
  NuisanceParameter: "TrigStat"
  Type: HISTO
  Samples: Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
  HistoNameUp: "prong_1_CentralRegion/TrigStat_UP"
  HistoNameDown: "prong_1_CentralRegion/TrigStat_DOWN"
  Symmetrisation: TwoSided
  Category: Trigger
  Regions: SR_ztt


Systematic: "TrigSyst"
  Title: "TrigSyst"
  NuisanceParameter: "TrigSyst"
  Type: HISTO
  ReferenceSample: Nom_Ztt
  ReferencePruning: Signal_0p00
  Samples: Signal_*p*
  HistoFileUp: "Ztt_PoPy8_0p0000"
  HistoFileDown: "Ztt_PoPy8_0p0000"
  HistoNameUp: "prong_1_CentralRegion/TrigSyst_UP"
  HistoNameDown: "prong_1_CentralRegion/TrigSyst_DOWN"
  Symmetrisation: TwoSided
  Category: Trigger
  Regions: SR_ztt

Systematic: "TrigSyst"
  Title: "TrigSyst"
  NuisanceParameter: "TrigSyst"
  Type: HISTO
  Samples: Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
  HistoNameUp: "prong_1_CentralRegion/TrigSyst_UP"
  HistoNameDown: "prong_1_CentralRegion/TrigSyst_DOWN"
  Symmetrisation: TwoSided
  Category: Trigger
  Regions: SR_ztt

### jet ###

#Systematic: "jet_forward_JVT"
#  Samples: Signal_*p*, Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
#  Regions: SR_ztt
#  Title: "jet_forward_JVT"
#  Type: HISTO
#  HistoNameUp: "prong_1_CentralRegion/jet_forward_JVT_UP"
#  HistoNameDown: "prong_1_CentralRegion/jet_forward_JVT_DOWN"
#  Category: Jet
#
#Systematic: "jet_central_JVT"
#  Samples: Signal_*p*, Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
#  Regions: SR_ztt
#  Title: "jet_central_JVT"
#  Type: HISTO
#  HistoNameUp: "prong_1_CentralRegion/jet_central_JVT_UP"
#  HistoNameDown: "prong_1_CentralRegion/jet_central_JVT_DOWN"
#  Category: Jet
#
#Systematic: "jet_FT_EFF_B_0"
#  Samples: Signal_*p*, Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
#  Regions: SR_ztt
#  Title: "jet_FT_EFF_B_0"
#  Type: HISTO
#  HistoNameUp: "prong_1_CentralRegion/jet_FT_EFF_B_0_UP"
#  HistoNameDown: "prong_1_CentralRegion/jet_FT_EFF_B_0_DOWN"
#  Category: Jet
#
#Systematic: "jet_FT_EFF_B_1"
#  Samples: Signal_*p*, Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
#  Regions: SR_ztt
#  Title: "jet_FT_EFF_B_1"
#  Type: HISTO
#  HistoNameUp: "prong_1_CentralRegion/jet_FT_EFF_B_1_UP"
#  HistoNameDown: "prong_1_CentralRegion/jet_FT_EFF_B_1_DOWN"
#  Category: Jet
#
#Systematic: "jet_FT_EFF_Light_0"
#  Samples: Signal_*p*, Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
#  Regions: SR_ztt
#  Title: "jet_FT_EFF_Light_0"
#  Type: HISTO
#  HistoNameUp: "prong_1_CentralRegion/jet_FT_EFF_Light_0_UP"
#  HistoNameDown: "prong_1_CentralRegion/jet_FT_EFF_Light_0_DOWN"
#  Category: Jet
#
#Systematic: "jet_FT_EFF_Light_1"
#  Samples: Signal_*p*, Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
#  Regions: SR_ztt
#  Title: "jet_FT_EFF_Light_1"
#  Type: HISTO
#  HistoNameUp: "prong_1_CentralRegion/jet_FT_EFF_Light_1_UP"
#  HistoNameDown: "prong_1_CentralRegion/jet_FT_EFF_Light_1_DOWN"
#  Category: Jet
#
#Systematic: "jet_FT_EFF_Light_2"
#  Samples: Signal_*p*, Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
#  Regions: SR_ztt
#  Title: "jet_FT_EFF_Light_2"
#  Type: HISTO
#  HistoNameUp: "prong_1_CentralRegion/jet_FT_EFF_Light_2_UP"
#  HistoNameDown: "prong_1_CentralRegion/jet_FT_EFF_Light_2_DOWN"
#  Category: Jet
#
#Systematic: "jet_FT_EFF_extrapolation_from_charm"
#  Samples: Signal_*p*, Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
#  Regions: SR_ztt
#  Title: "jet_FT_EFF_extrapolation_from_charm"
#  Type: HISTO
#  HistoNameUp: "prong_1_CentralRegion/jet_FT_EFF_extrapolation_from_charm_UP"
#  HistoNameDown: "prong_1_CentralRegion/jet_FT_EFF_extrapolation_from_charm_DOWN"
#
### muon ###

Systematic: "lep_Reco_Syst"
  Title: "lep_Reco_Syst"
  NuisanceParameter: "lep_Reco_Syst"
  Type: HISTO
  ReferenceSample: Nom_Ztt
  ReferencePruning: Signal_0p00
  Samples: Signal_*p*
  HistoFileUp: "Ztt_PoPy8_0p0000"
  HistoFileDown: "Ztt_PoPy8_0p0000"
  HistoNameUp: "prong_1_CentralRegion/lep_Reco_SYS_UP"
  HistoNameDown: "prong_1_CentralRegion/lep_Reco_SYS_DOWN"
  Symmetrisation: TwoSided
  Category: Muon
  Regions: SR_ztt

Systematic: "lep_Reco_Syst"
  Title: "lep_Reco_Syst"
  NuisanceParameter: "lep_Reco_Syst"
  Type: HISTO
  Samples: Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
  HistoNameUp: "prong_1_CentralRegion/lep_Reco_SYS_UP"
  HistoNameDown: "prong_1_CentralRegion/lep_Reco_SYS_DOWN"
  Symmetrisation: TwoSided
  Category: Muon
  Regions: SR_ztt


Systematic: "lep_Reco_Stat"
  Title: "lep_Reco_Stat"
  NuisanceParameter: "lep_Reco_Stat"
  Type: HISTO
  ReferenceSample: Nom_Ztt
  ReferencePruning: Signal_0p00
  Samples: Signal_*p*
  HistoFileUp: "Ztt_PoPy8_0p0000"
  HistoFileDown: "Ztt_PoPy8_0p0000"
  HistoNameUp: "prong_1_CentralRegion/lep_Reco_STAT_UP"
  HistoNameDown: "prong_1_CentralRegion/lep_Reco_STAT_DOWN"
  Symmetrisation: TwoSided
  Category: Muon
  Regions: SR_ztt

Systematic: "lep_Reco_Stat"
  Title: "lep_Reco_Stat"
  NuisanceParameter: "lep_Reco_Stat"
  Type: HISTO
  Samples: Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
  HistoNameUp: "prong_1_CentralRegion/lep_Reco_STAT_UP"
  HistoNameDown: "prong_1_CentralRegion/lep_Reco_STAT_DOWN"
  Symmetrisation: TwoSided
  Category: Muon
  Regions: SR_ztt


Systematic: "lep_TTVA_Syst"
  Title: "lep_TTVA_Syst"
  NuisanceParameter: "lep_TTVA_Syst"
  Type: HISTO
  ReferenceSample: Nom_Ztt
  ReferencePruning: Signal_0p00
  Samples: Signal_*p*
  HistoFileUp: "Ztt_PoPy8_0p0000"
  HistoFileDown: "Ztt_PoPy8_0p0000"
  HistoNameUp: "prong_1_CentralRegion/lep_TTVA_SYS_UP"
  HistoNameDown: "prong_1_CentralRegion/lep_TTVA_SYS_DOWN"
  Symmetrisation: TwoSided
  Category: Muon
  Regions: SR_ztt

Systematic: "lep_TTVA_Syst"
  Title: "lep_TTVA_Syst"
  NuisanceParameter: "lep_TTVA_Syst"
  Type: HISTO
  Samples: Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
  HistoNameUp: "prong_1_CentralRegion/lep_TTVA_SYS_UP"
  HistoNameDown: "prong_1_CentralRegion/lep_TTVA_SYS_DOWN"
  Symmetrisation: TwoSided
  Category: Muon
  Regions: SR_ztt


Systematic: "lep_TTVA_Stat"
  Title: "lep_TTVA_Stat"
  NuisanceParameter: "lep_TTVA_Stat"
  Type: HISTO
  ReferenceSample: Nom_Ztt
  ReferencePruning: Signal_0p00
  Samples: Signal_*p*
  HistoFileUp: "Ztt_PoPy8_0p0000"
  HistoFileDown: "Ztt_PoPy8_0p0000"
  HistoNameUp: "prong_1_CentralRegion/lep_TTVA_STAT_UP"
  HistoNameDown: "prong_1_CentralRegion/lep_TTVA_STAT_DOWN"
  Symmetrisation: TwoSided
  Category: Muon
  Regions: SR_ztt

Systematic: "lep_TTVA_Stat"
  Title: "lep_TTVA_Stat"
  NuisanceParameter: "lep_TTVA_Stat"
  Type: HISTO
  Samples: Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
  HistoNameUp: "prong_1_CentralRegion/lep_TTVA_STAT_UP"
  HistoNameDown: "prong_1_CentralRegion/lep_TTVA_STAT_DOWN"
  Symmetrisation: TwoSided
  Category: Muon
  Regions: SR_ztt


Systematic: "lep_Iso_Syst"
  Title: "lep_Iso_Syst"
  NuisanceParameter: "lep_Iso_Syst"
  Type: HISTO
  ReferenceSample: Nom_Ztt
  ReferencePruning: Signal_0p00
  Samples: Signal_*p*
  HistoFileUp: "Ztt_PoPy8_0p0000"
  HistoFileDown: "Ztt_PoPy8_0p0000"
  HistoNameUp: "prong_1_CentralRegion/lep_Iso_SYS_UP"
  HistoNameDown: "prong_1_CentralRegion/lep_Iso_SYS_DOWN"
  Symmetrisation: TwoSided
  Category: Muon
  Regions: SR_ztt

Systematic: "lep_Iso_Syst"
  Title: "lep_Iso_Syst"
  NuisanceParameter: "lep_Iso_Syst"
  Type: HISTO
  Samples: Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
  HistoNameUp: "prong_1_CentralRegion/lep_Iso_SYS_UP"
  HistoNameDown: "prong_1_CentralRegion/lep_Iso_SYS_DOWN"
  Symmetrisation: TwoSided
  Category: Muon
  Regions: SR_ztt


Systematic: "lep_Iso_Stat"
  Title: "lep_Iso_Stat"
  NuisanceParameter: "lep_Iso_Stat"
  Type: HISTO
  ReferenceSample: Nom_Ztt
  ReferencePruning: Signal_0p00
  Samples: Signal_*p*
  HistoFileUp: "Ztt_PoPy8_0p0000"
  HistoFileDown: "Ztt_PoPy8_0p0000"
  HistoNameUp: "prong_1_CentralRegion/lep_Iso_STAT_UP"
  HistoNameDown: "prong_1_CentralRegion/lep_Iso_STAT_DOWN"
  Symmetrisation: TwoSided
  Category: Muon
  Regions: SR_ztt

Systematic: "lep_Iso_Stat"
  Title: "lep_Iso_Stat"
  NuisanceParameter: "lep_Iso_Stat"
  Type: HISTO
  Samples: Zll_fake, Wl_fake, Wt_fake, TTbar_fake, STbar_fake
  HistoNameUp: "prong_1_CentralRegion/lep_Iso_STAT_UP"
  HistoNameDown: "prong_1_CentralRegion/lep_Iso_STAT_DOWN"
  Symmetrisation: TwoSided
  Category: Muon
  Regions: SR_ztt


### tau ###


### Electron ##
#Systematic: "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR"
#  Title: "Ele Trigger Eff"
#  Type: HISTO
#  HistoNameSufUp: "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up"
#  HistoNameSufDown: "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down"
#  Category: Electron
#
#Systematic: "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR"
#  Title: "Ele Reco Total Eff"
#  Type: HISTO
#  HistoNameSufUp: "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1up"
#  HistoNameSufDown: "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1down"
#  Category: Electron
#
#Systematic: "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR"
#  Title: "Ele ID Total Eff"
#  Type: HISTO
#  HistoNameSufUp: "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up"
#  HistoNameSufDown: "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1down"
#  Category: Electron
#
#Systematic: "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR"
#  Title: "Ele Iso Total Eff"
#  Type: HISTO
#  HistoNameSufUp: "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1up"
#  HistoNameSufDown: "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1down"
#  Category: Electron
#
#Systematic: "EL_CHARGEID_STAT"
#  Title: "Ele Charge ID Stat"
#  Type: HISTO
#  HistoNameSufUp: "EL_CHARGEID_STAT_1up"
#  HistoNameSufDown: "EL_CHARGEID_STAT_1down"
#  Category: Electron
#
#Systematic: "EL_CHARGEID_SYStotal"
#  Title: "Ele Charge ID Syst"
#  Type: HISTO
#  HistoNameSufUp: "EL_CHARGEID_SYStotal_1up"
#  HistoNameSufDown: "EL_CHARGEID_SYStotal_1down"
#  Category: Electron
#
### Muon ##
#Systematic: "MUON_EFF_TrigStatUncertainty"
#  Title: "Muon Trigger Eff Stat"
#  Type: HISTO
#  HistoNameSufUp: "MUON_EFF_TrigStatUncertainty_1up"
#  HistoNameSufDown: "MUON_EFF_TrigStatUncertainty_1down"
#  Category: Muon
#
#Systematic: "MUON_EFF_TrigSystUncertainty"
#  Title: "Muon Trigger Eff Syst"
#  Type: HISTO
#  HistoNameSufUp: "MUON_EFF_TrigSystUncertainty_1up"
#  HistoNameSufDown: "MUON_EFF_TrigSystUncertainty_1down"
#  Category: Muon
#
#Systematic: "MUON_EFF_RECO_STAT"
#  Title: "Muon Reco Eff Stat"
#  Type: HISTO
#  HistoNameSufUp: "MUON_EFF_RECO_STAT_1up"
#  HistoNameSufDown: "MUON_EFF_RECO_STAT_1down"
#  Category: Muon
#
#Systematic: "MUON_EFF_RECO_SYS"
#  Title: "Muon Reco Eff Syst"
#  Type: HISTO
#  HistoNameSufUp: "MUON_EFF_RECO_SYS_1up"
#  HistoNameSufDown: "MUON_EFF_RECO_SYS_1down"
#  Category: Muon
#
#Systematic: "MUON_EFF_RECO_STAT_LOWPT"
#  Title: "Muon Low-p_{T} Reco Eff Stat"
#  Type: HISTO
#  HistoNameSufUp: "MUON_EFF_RECO_STAT_LOWPT_1up"
#  HistoNameSufDown: "MUON_EFF_RECO_STAT_LOWPT_1down"
#  Category: Muon
#
#Systematic: "MUON_EFF_RECO_SYS_LOWPT"
#  Title: "Muon Low-p_{T} Reco Eff Syst"
#  Type: HISTO
#  HistoNameSufUp: "MUON_EFF_RECO_SYS_LOWPT_1up"
#  HistoNameSufDown: "MUON_EFF_RECO_SYS_LOWPT_1down"
#  Category: Muon
#
#Systematic: "MUON_EFF_ISO_STAT"
#  Title: "Muon Iso Eff Stat"
#  Type: HISTO
#  HistoNameSufUp: "MUON_EFF_ISO_STAT_1up"
#  HistoNameSufDown: "MUON_EFF_ISO_STAT_1down"
#  Category: Muon
#
#Systematic: "MUON_EFF_ISO_SYS"
#  Title: "Muon Iso Eff Syst"
#  Type: HISTO
#  HistoNameSufUp: "MUON_EFF_ISO_SYS_1up"
#  HistoNameSufDown: "MUON_EFF_ISO_SYS_1down"
#  Category: Muon
### no muon ID? Is that right? Double-check!
#
### Flavour Tagging ##
#Systematic: "FT_EFF_Eigen_B_0"
#  Title: "Jet FTAG B 0"
#  Type: HISTO
#  HistoNameSufUp: "FT_EFF_Eigen_B_0_1up"
#  HistoNameSufDown: "FT_EFF_Eigen_B_0_1down"
#  Category: Jet
#  SubCategory: FTAG
#
#Systematic: "FT_EFF_Eigen_B_1"
#  Title: "Jet FTAG B 1"
#  Type: HISTO
#  HistoNameSufUp: "FT_EFF_Eigen_B_1_1up"
#  HistoNameSufDown: "FT_EFF_Eigen_B_1_1down"
#  Category: Jet
#  SubCategory: FTAG
#
#Systematic: "FT_EFF_Eigen_B_2"
#  Title: "Jet FTAG B 2"
#  Type: HISTO
#  HistoNameSufUp: "FT_EFF_Eigen_B_2_1up"
#  HistoNameSufDown: "FT_EFF_Eigen_B_2_1down"
#  Category: Jet
#  SubCategory: FTAG
#
#Systematic: "FT_EFF_Eigen_C_0"
#  Title: "Jet FTAG C 0"
#  Type: HISTO
#  HistoNameSufUp: "FT_EFF_Eigen_C_0_1up"
#  HistoNameSufDown: "FT_EFF_Eigen_C_0_1down"
#  Category: Jet
#  SubCategory: FTAG
#
#Systematic: "FT_EFF_Eigen_C_1"
#  Title: "Jet FTAG C 1"
#  Type: HISTO
#  HistoNameSufUp: "FT_EFF_Eigen_C_1_1up"
#  HistoNameSufDown: "FT_EFF_Eigen_C_1_1down"
#  Category: Jet
#  SubCategory: FTAG
#
#Systematic: "FT_EFF_Eigen_C_2"
#  Title: "Jet FTAG C 2"
#  Type: HISTO
#  HistoNameSufUp: "FT_EFF_Eigen_C_2_1up"
#  HistoNameSufDown: "FT_EFF_Eigen_C_2_1down"
#  Category: Jet
#  SubCategory: FTAG
#
#Systematic: "FT_EFF_Eigen_Light_0"
#  Title: "Jet FTAG Light 0"
#  Type: HISTO
#  HistoNameSufUp: "FT_EFF_Eigen_Light_0_1up"
#  HistoNameSufDown: "FT_EFF_Eigen_Light_0_1down"
#  Category: Jet
#  SubCategory: FTAG
#
#Systematic: "FT_EFF_Eigen_Light_1"
#  Title: "Jet FTAG Light 1"
#  Type: HISTO
#  HistoNameSufUp: "FT_EFF_Eigen_Light_1_1up"
#  HistoNameSufDown: "FT_EFF_Eigen_Light_1_1down"
#  Category: Jet
#  SubCategory: FTAG
#
#Systematic: "FT_EFF_Eigen_Light_2"
#  Title: "Jet FTAG Light 2"
#  Type: HISTO
#  HistoNameSufUp: "FT_EFF_Eigen_Light_2_1up"
#  HistoNameSufDown: "FT_EFF_Eigen_Light_2_1down"
#  Category: Jet
#  SubCategory: FTAG
#
#Systematic: "FT_EFF_Eigen_Light_3"
#  Title: "Jet FTAG Light 3"
#  Type: HISTO
#  HistoNameSufUp: "FT_EFF_Eigen_Light_3_1up"
#  HistoNameSufDown: "FT_EFF_Eigen_Light_3_1down"
#  Category: Jet
#  SubCategory: FTAG
#
#Systematic: "FT_EFF_extrapolation"
#  Title: "Jet FTAG Extrapolation"
#  Type: HISTO
#  HistoNameSufUp: "FT_EFF_extrapolation_1up"
#  HistoNameSufDown: "FT_EFF_extrapolation_1down"
#  Category: Jet
#  SubCategory: FTAG
#
#Systematic: "FT_EFF_extrapolation_from_charm"
#  Title: "Jet FTAG Charm Extrap"
#  Type: HISTO
#  HistoNameSufUp: "FT_EFF_extrapolation_from_charm_1up"
#  HistoNameSufDown: "FT_EFF_extrapolation_from_charm_1down"
#  Category: Jet
#  SubCategory: FTAG
#
### other jet-related ##
#
#Systematic: "JET_fJvtEfficiency"
#  Title: "Jet fJVT Eff"
#  Type: HISTO
#  HistoNameSufUp: "JET_fJvtEfficiency_1up"
#  HistoNameSufDown: "JET_fJvtEfficiency_1down"
#  Category: Jet
#  SubCategory: Reco
#
#Systematic: "JET_JvtEfficiency"
#  Title: "Jet JVT Eff"
#  Type: HISTO
#  HistoNameSufUp: "JET_JvtEfficiency_1up"
#  HistoNameSufDown: "JET_JvtEfficiency_1down"
#  Category: Jet
#  SubCategory: Reco

# lumi makes no sense yet, wait until truth splitting is in
#Systematic: "LUMINOSITY"
#  Title: "Luminosity"
#  Type: OVERALL
#  OverallUp: 0.017
#  OverallDown: -0.017
#  Category: LUMI

