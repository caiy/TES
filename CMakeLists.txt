# CMakeLists.txt for event package. It creates a library with dictionary and a main program
cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
project(Selector)

# You need to tell CMake where to find the ROOT installation. This can be done in a number of ways:
#   - ROOT built with classic configure/make use the provided $ROOTSYS/etc/cmake/FindROOT.cmake
#   - ROOT built with CMake. Add in CMAKE_PREFIX_PATH the installation prefix for ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

#---Locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS)
find_package(ROOT)

set(BOOST_ROOT "/cvmfs/sft.cern.ch/lcg/releases/Boost/1.72.0-7bbce/x86_64-centos7-gcc8-opt")
set(Boost_NO_BOOST_CMAKE ON)
FIND_PACKAGE( Boost 1.70 COMPONENTS program_options REQUIRED )

#Check if Root included
if(!ROOT_FOUND)
	message(FATAL_ERROR "Fetal: Root pacakge not found!")
endif()

#Shared libary files
aux_source_directory(./ DIR_SRCS)

#---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
#We use TROOT.h, so we need ROOT_USE_FILE
include(${ROOT_USE_FILE})
include_directories(./ ./include/ ./nlohmann/include/ ${Boost_INCLUDE_DIR})

#---Create  a main program using the library
add_executable(mini_analysis ${DIR_SRCS} )
target_link_libraries(mini_analysis stdc++fs ${ROOT_LIBRARIES} ${Boost_LIBRARIES})
