#pragma once
#include <iostream>
#include <fstream>
#include <algorithm>
#include <functional>
#include <memory>
#include <string>
#include <vector>
#include <cmath>

#include "Runnable.h"
#include "Utils.h"

class FitFunction : public Runnable {
public:

    FitFunction() {
    };

    FitFunction(std::string chainName, std::string histName, double LearnRate , double StopEpsilon , double MomentumGamma , std::vector<double> DataNom, std::vector<double> BkgNom, std::vector<double> BkgErr, std::vector<double> SigNom, std::vector<double> SigErr, double* chi, double* factor, double* factorErr){
        setAnaChain(chainName, histName, LearnRate, StopEpsilon, MomentumGamma, DataNom, BkgNom, BkgErr, SigNom, SigErr, chi, factor, factorErr) ;
    }

    void setAnaChain(std::string chainName, std::string histName, double LearnRate , double StopEpsilon , double MomentumGamma , std::vector<double> DataNom, std::vector<double> BkgNom, std::vector<double> BkgErr, std::vector<double> SigNom, std::vector<double> SigErr, double* chi, double* factor, double* factorErr ) {
        // lock area
        std::lock_guard<std::mutex> lock{_lock};
        this->alpha_name = chainName ;
        this->hist_name = histName ;
        this->learn_rate = LearnRate ;
        this->stop_epsilon = StopEpsilon;
        this->momentum = MomentumGamma ;
        this->data_yields = DataNom;
        this->bkg_yields = BkgNom;
        this->bkg_error = BkgErr;
        this->sig_yields = SigNom;
        this->sig_error = SigErr;
        this->best_Chi2 = chi;
        this->best_factor = factor;
        this->factor_error = factorErr;
    }
    
    void run() override {
        std::cout << "run FitFunction:" << this->alpha_name << "; "<< std::endl;

        std::vector<double> chi2_vec = {} ;
        double theta_i_1 = 100;
        double theta_i = 0.5;
        double nu_i = 0;
        double nu_i_i = 0;
        int iter_num = 0;
        double current_chi2 = 0;
        while ( fabs( CalChi2(theta_i) - CalChi2(theta_i_1) ) > this->stop_epsilon ){
            theta_i_1 = theta_i ; 
            nu_i_i = nu_i ;
            nu_i = this->momentum*nu_i_i - this->learn_rate*CalGradientChi2(theta_i_1) ;
            theta_i = theta_i_1 + nu_i ;
            iter_num ++;
            current_chi2 = CalChi2(theta_i);
            chi2_vec.push_back(current_chi2);
            std::cout<<"f-floatfactor:"<<theta_i<<"="<<theta_i_1<<"+"<<nu_i<<" ; Chi2:"<<current_chi2<<std::endl;
        }
        
        double d_alpha =  AlphaToDouble(alpha_name) ;
        //if( this->hist_name.find("Nominal") != std::string::npos ){
        //    std::ofstream epoch_file;
        //    epoch_file.open("SaveGrid.txt", std::fstream::app);
        //    //epoch_file.open("SaveGrid_"+this->alpha_name+"_"+this->hist_name+".txt");
        //    epoch_file << std::to_string(d_alpha) + " , " + std::to_string(theta_i) + " , " + std::to_string(current_chi2) + '\n' ;
        //    int i = 0;
        //    while( i < 300 ){
        //        i++ ;
        //        double f_factor_up = theta_i + 0.0001 * i ;
        //        double f_factor_dw = theta_i - 0.0001 * i ;
        //        epoch_file << std::to_string(d_alpha)+ " , " + std::to_string(f_factor_up) + " , " + std::to_string(CalChi2(f_factor_up) ) + '\n';
        //        epoch_file << std::to_string(d_alpha)+ " , " + std::to_string(f_factor_dw) + " , " + std::to_string(CalChi2(f_factor_dw) ) + '\n';
        //    }
        //    epoch_file.close();
        //}
        double scan_range = 0.01 ;
        double step = 0.0001 ;
        int i = 0;
        while( i*step < 2*scan_range ){
            double theta_alter = theta_i - scan_range + i * step ;
            if( CalChi2(theta_alter) < current_chi2 ){
                current_chi2 = CalChi2(theta_alter) ;
                theta_i = theta_alter ;
            }
            i++ ;
        }
        if( alpha_name.find("0p0000") != std::string::npos ){
            std::ofstream epoch_file;
            epoch_file.open(this->alpha_name+"_"+this->hist_name+".txt");
            for( int i = 0 ; i < chi2_vec.size() ; i++ ){
                epoch_file << chi2_vec.at(i) << " , " ;
            }
            epoch_file.close();
        }
        if( alpha_name.find("0p0000") != std::string::npos || alpha_name.find("m0p0800") != std::string::npos ){
            std::ofstream hist_file;
            hist_file.open("hist_"+alpha_name+"_"+this->hist_name+".txt");
            //hist_file.open("hist_"+this->hist_name+".txt");
            
            for( int i = 0 ; i < this->data_yields.size() - 1 ; i++ ){
                hist_file << this->data_yields.at(i) << " , " ;
            }
            hist_file << this->data_yields.at(this->data_yields.size() - 1) << std::endl;
            
            for( int i = 0 ; i < this->bkg_yields.size() - 1 ; i++ ){
                hist_file << this->bkg_yields.at(i) << " , " ;
            }
            hist_file << this->bkg_yields.at(this->bkg_yields.size() - 1) << std::endl;
            
            for( int i = 0 ; i < this->bkg_error.size() - 1 ; i++ ){
                hist_file << this->bkg_error.at(i) << " , " ;
            }
            hist_file << this->bkg_error.at(this->bkg_error.size() - 1) << std::endl;
            
            for( int i = 0 ; i < this->sig_yields.size() - 1 ; i++ ){
                hist_file << this->sig_yields.at(i) << " , " ;
            }
            hist_file << this->sig_yields.at(this->sig_yields.size() - 1) << std::endl;
            
            for( int i = 0 ; i < this->sig_error.size() - 1 ; i++ ){
                hist_file << this->sig_error.at(i) << " , " ;
            }
            hist_file << this->sig_error.at(this->sig_error.size() - 1) << std::endl;
            
            hist_file.close();
        }
        
        //current_chi2 = CalChi2(1.) ; //Fixed float factor;
        *this->best_Chi2 = current_chi2;
        *this->best_factor = theta_i ;
        *this->factor_error = CalError(theta_i , current_chi2 );
    }

    double CalError(double f_factor, double min_chi2){
        
        //find the uncertainty of factor
        double factor_high_up = f_factor + 0.10;
        double factor_high_dw = f_factor;
        double factor_low_up = f_factor;
        double factor_low_dw = f_factor - 0.10;
        double target_chi2 = min_chi2 + 1 ;
        while( (factor_high_up-factor_high_dw)>0.00001 ){
            double factor_high_mid = (factor_high_up+factor_high_dw)/2;
            if ( CalChi2(factor_high_mid) > target_chi2 ){
                factor_high_up = factor_high_mid;
            }else{
                factor_high_dw = factor_high_mid;
            }
        }
        while( (factor_low_up-factor_low_dw)>0.00001 ){
            double factor_low_mid = (factor_low_up+factor_low_dw)/2;
            if ( CalChi2(factor_low_mid) > target_chi2 ){
                factor_low_dw = factor_low_mid;
            }else{
                factor_low_up = factor_low_mid;
            }
        }
        //std::cout<<min_chi2<<"-"<<CalChi2(0.5 * (factor_high_up + factor_low_dw))<<" = "<< min_chi2 - CalChi2(0.5 * (factor_high_up + factor_low_dw))<<std::endl; 
        //*this->best_factor = 0.5 * (factor_high_up + factor_low_dw) ;
        //*this->best_Chi2 = CalChi2(0.5 * (factor_high_up + factor_low_dw) ) ;
        // re-calculate best f-factor and chi2 if calculate error
        
        double err_factor = 0.5 * (factor_high_up - factor_low_dw);
        return err_factor;
    }

    double CalGradientChi2(double f_factor) {
        double SumGradient = 0;
        for(int i=0 ; i < this->data_yields.size() ; i ++){
            double A = data_yields.at(i)-bkg_yields.at(i)-f_factor*sig_yields.at(i) ;
            double B = data_yields.at(i) + std::pow(bkg_error.at(i),2) + std::pow(f_factor*sig_error.at(i),2) ;
            SumGradient += (2*A/(B*B)) * ( A*sig_error.at(i)*sig_error.at(i)*f_factor - B*sig_yields.at(i) ) ;
        }
        return SumGradient;
    }

    double CalChi2(double f_factor) {
        double SumChi2 = 0;
        for(int i=0 ; i < this->data_yields.size() ; i ++){
            SumChi2 += std::pow( data_yields.at(i)-bkg_yields.at(i)-f_factor*sig_yields.at(i) , 2) / ( data_yields.at(i) + std::pow(bkg_error.at(i),2) + std::pow(f_factor*sig_error.at(i),2) ) ;
            //std::cout << "bin"<<i<<" D:"<<data_yields.at(i)<<" B:"<<bkg_yields.at(i)<<" S:"<<f_factor*sig_yields.at(i)<<" ; ";
        }
        //std::cout << " *f:"<<f_factor<< std::endl;
        return SumChi2;
    }
    
    double AlphaToDouble( std::string alpha ){
        
        size_t pos;
        if ( (pos = alpha.find("p")) != std::string::npos ){
            alpha.replace(pos,1,".");
        }
        if ( alpha.find("m") != std::string::npos ){
            alpha.replace(0, 1, "-");
        }
        double alpha_num = std::stod(alpha);
        return alpha_num ;
    }

    virtual ~FitFunction() {
        std::lock_guard<std::mutex> lock{_lock};
        //std::cout << "end of " << this->alpha_name << std::endl;
        // delete chain
        //SafeDelete(anaChain);
        // write and delete alloc trees
    }

private:
    std::mutex _lock; // for asyn
    std::string alpha_name;
    std::string hist_name;
    
    double learn_rate ;
    double stop_epsilon ;
    double momentum ;
    std::vector<double> data_yields;
    std::vector<double> bkg_yields;
    std::vector<double> bkg_error;
    std::vector<double> sig_yields;
    std::vector<double> sig_error;
    double* best_Chi2;
    double* best_factor;
    double* factor_error;
};
