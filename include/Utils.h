#pragma once
#include <TFile.h>
#include <TKey.h>
#include <TTree.h>

#include <filesystem>
#include <fstream>
#include <initializer_list>
#include <iomanip>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

class Utils {
public:
    /***************************** Inline func and template func *******************************/
    // Get the how many digit before the point, for 0.XXX, will return 0: getIntDigit(94735.2) = 5, getIntDigit(345.12)
    // = 3
    static int getIntDigit(double num) {
        double absNum = fabs(num);
        if (absNum < 1) {
            return 0;
        }
        return (int)(log10(absNum) + 1);
    }

    /********* convert anyType to a specific type *****
     * To use eg: std::string s = "34553.353";
     * double d = Tools::convert<double>(s);
     **************************************************/
    template <class out_type, class in_type> static out_type convert(in_type& src) {
        std::stringstream ss;
        out_type result;
        ss << src;
        ss >> result;
        return result;
    }

    /********* find the position in the list where *****
     * "value > list[posi] && value <= list[posi + 1]
     * using Interpolation Search. Will return -1 is smaller than all of the value
     * ************************************************/
    template <typename T> static int positionSearch(const T& value, const std::vector<T>& vec) {
        int right = vec.size() - 1;
        int left = 0;

        while (right - left != 1) {
            auto varright = vec.at(right);
            auto varleft = vec.at(left);
            if (value <= varleft) return left - 1;
            if (value > varright) return right;
            int mid = left + ((float)value - varleft) / (varright - varleft) * (right - left);
            // To avoid there is no any step movement, like var == left + very small varlue or var == right
            if (mid == left) mid = left + 1;
            if (mid == right) mid = right - 1;
            if (value > vec.at(mid)) {
                left = mid;
            } else {
                right = mid;
            }
        }
        return left;
    }

    /********* to_string but with precision *****
     */
    template <typename T>
    static std::string to_string_with_precision(const T a_value, const int n = 6) {
        std::ostringstream out;
        out.precision(n);
        out << std::fixed << a_value;
        return out.str();
    }

    //********* Round the num and err simply***************
    static void roundCSV(std::ofstream& stream, double num, double err);
    static void roundLatex(std::ofstream& stream, double num, double err);

    //********* get the sums of CSV table******************
    static std::tuple<double, double> addYields(std::tuple<double, double> yields1, std::tuple<double, double> yields2);
    static std::vector<std::vector<std::tuple<double, double>>> getYieldsTable(std::string filename,
                                                                               std::initializer_list<std::string> excludeNameReg = {},
                                                                               std::initializer_list<std::string> includeNameReg = {});
    static std::string getTotalYields(std::string filename, std::initializer_list<std::string> excludeNameReg = {},
                                      std::initializer_list<std::string> includeNameReg = {});

    //******* erase string form a certain str to the end***
    static std::string eraseToEnd(std::string target, std::string src);

    //******* generate a int vector in a given range with certain step
    static std::vector<int> vecRange(int end);
    static std::vector<int> vecRange(int begin, int end, int step = 1);

    //******* split a string by a char symbol **************
    static std::vector<std::string> splitStrBy(std::string names, char symbol);

    /******* Check if a 'ip' matches the regex 'pattern' **********
     * Will return a string vector. the vec[0] will be the matched string if match
     * Then following the captured words if regex contains
     * Why the patttern don't use 'string' type? Because it may be call in a loop while regex
     * construct cost lots of time. It's recommended to construct it by yourself outside a loop
     */
    static std::vector<std::string> regexMatch(const std::regex& pattern, std::string ip);

    //******* select files which name matches the regex in the folder
    static std::vector<std::filesystem::path> getFiles(std::string folder, std::string pattern = "");
    //******* select files which name matches the regex in the folder with the a
    //******* vector to collect the extra Capture
    static std::vector<std::filesystem::path> getFiles(std::string folder, std::string pattern, std::vector<std::vector<std::string>>& extraCapture);

    //******* get the tree list of a file
    static std::vector<std::string> getTreeList(std::string filename, std::string filter = "");
    //****** reserve X digit *******
    static double reserveDigit(double num, unsigned int digist);
};
