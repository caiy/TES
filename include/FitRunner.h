#include <functional>
#include <memory>
#include <TFile.h>
#include <TH1.h>
#include <TF1.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TROOT.h>
#include <TLatex.h>
#include <TLine.h>
#include <TFitResult.h>

#include "AnaRunner.h"
#include "ConfigHolder.h"
#include "FitFunction.h"
#include "HistLoader.h"
#include "ThreadPool.h"
#include "Utils.h"

class FitRunner : public AnaRunner {
public:
    using FitFunction_ptr = std::unique_ptr<FitFunction>;
    using HistLoader_ptr = std::unique_ptr<HistLoader>;

    FitRunner(std::function<FitFunction_ptr(std::string chainName, std::string histName, double LearnRate , double StopEpsilon , double MomentumGamma , std::vector<double> DataNom, std::vector<double> BkgNom, std::vector<double> BkgErr, std::vector<double> SigNom, std::vector<double> SigErr, double* chi, double* factor, double* factorErr)> looper) { this->getLooper = looper; }

    void runChains(ConfigHolder config) override {
        // check if we need the ntuple output
        this->config = config ;
        //std::string folder_path = config.hist_name_map["Path"];
        std::string SIG_name    = config.hist_name_map["SIG"];
        std::string hist_name   = config.hist_name_map["DATA"] ; 
        std::string QCD_name    = config.hist_name_map["QCD"] ;
        std::string BKG_name    = config.hist_name_map["BKG"] ;

        ReadData(config.getData(), hist_name, config.ActivePeriod);
        this->hist_name_map["DATA"]=hist_name;
        this->hist_name_map["QCD"]=QCD_name;
        this->hist_name_map["BKG"]=BKG_name;
        this->hist_name_map["SIG"]=SIG_name;
        this->output_file_name = BKG_name+config.hist_name_map["Out"];
        std::replace( this->output_file_name.begin(), this->output_file_name.end(), '/', '_');

        StoreAlpha(config.alpha_collection);
        StoreParameter( config.LearnRate , config.Stop , config.MomentumGamma );

        int chainSize = this->alpha_names.size();
        unsigned int threadNum = 10;
        std::cout << "Running with " << threadNum << " thread" << ", Total chain "<< chainSize << std::endl;

        //ReadBkg();

        //start to load Histogram
        threadNum = 1;
        if (threadNum == 1) {
            for (const auto& chainname : this->alpha_names) {
                std::cout<<"reading hist for chain: "<<chainname<<std::endl;
                HistLoader_ptr hist_loader(new HistLoader (chainname, config, &this->sig_nom_map[chainname], &this->sig_err_map[chainname], &this->bkg_nom_map[chainname], &this->bkg_err_map[chainname]) );
                hist_loader->run();
                //HistLoader_ptr hist_loader(new HistLoader ("0p0000", hist_name_map, &this->sig_nom_map["0p0000"], &this->sig_err_map["0p0000"], &this->bkg_nom_map["0p0000"], &this->bkg_err_map["0p0000"]) );
                //hist_loader->run();
            }
        } else {
            
            ROOT::EnableThreadSafety();

            std::unique_ptr<ThreadPool> pool(new ThreadPool(threadNum));
            for (const auto& chainname : this->alpha_names) {
                std::cout<<"reading hist for chain: "<<chainname<<std::endl;
                pool->addTask(std::move(HistLoader_ptr(new HistLoader(chainname, config, &this->sig_nom_map[chainname], &this->sig_err_map[chainname], &this->bkg_nom_map[chainname], &this->bkg_err_map[chainname]) )));
            }
            pool->shutDownPool();
            std::cout << "**********All*********" << std::endl ;
        }

        std::cout << "*******************" << "finish reading hist" << "*******************"<< std::endl ;
        threadNum = 1;
        if (threadNum == 1) {
            for (const auto& chainname : this->alpha_names) {
                std::cout<<"DATA"<<std::endl;
                for (int i = 0; i < this->data_nom_map.size(); i++) {
                    std::cout<<this->data_nom_map.at(i)<<" ";
                }
                std::cout<<std::endl;
                std::cout<<"SIG"<<std::endl;
                for (int i = 0; i < this->sig_nom_map[""].size(); i++) {
                    std::cout<<this->sig_nom_map[""].at(i)<<" ";
                }
                std::cout<<std::endl;
                std::cout<<"BKG"<<std::endl;
                for (int i = 0; i < this->bkg_nom_map[""].size(); i++) {
                    std::cout<<this->bkg_nom_map[""].at(i)<<" ";
                }
                std::cout<<std::endl;
                FitFunction_ptr looper = getLooper(chainname, output_file_name, this->LearnRate, this->StopEpsilon, this->MomentumGamma, this->data_nom_map, this->bkg_nom_map[chainname], this->bkg_err_map[chainname], this->sig_nom_map[chainname], this->sig_err_map[chainname], &this->alpha_chi2[chainname], &this->alpha_factor_Nom[chainname], &this->alpha_factor_Err[chainname]);
                looper->run();
                //FitFunction_ptr looper = getLooper("0p0000", output_file_name, this->LearnRate, this->StopEpsilon, this->MomentumGamma, this->data_nom_map, this->bkg_nom_map["0p0000"], this->bkg_err_map["0p0000"], this->sig_nom_map["0p0000"], this->sig_err_map["0p0000"], &this->alpha_chi2["0p0000"], &this->alpha_factor_Nom["0p0000"], &this->alpha_factor_Err["0p0000"]);
                //looper->run();
            }
        } else {
            std::unique_ptr<ThreadPool> pool(new ThreadPool(threadNum));
            for (const auto& chainname : this->alpha_names) {
                pool->addTask(std::move(getLooper(chainname, output_file_name, this->LearnRate, this->StopEpsilon, this->MomentumGamma, this->data_nom_map, this->bkg_nom_map[chainname], this->bkg_err_map[chainname], this->sig_nom_map[chainname], this->sig_err_map[chainname], &this->alpha_chi2[chainname], &this->alpha_factor_Nom[chainname], &this->alpha_factor_Err[chainname])));
            }
            pool->shutDownPool();
            std::cout << "**********All thread done. Delete pools and Write all obj*********" << std::endl ;
        }

        MakeChi2Plot(this->data_nom_map.size() );
    }


    std::function<FitFunction_ptr(std::string chainname, std::string histName, double LearnRate, double StopEpsilon, double MomentumGamma, std::vector<double> DataNom, std::vector<double> BkgNom, std::vector<double> BkgErr, std::vector<double> SigNom, std::vector<double> SigErr, double* chi, double* factor, double* factorErr)> getLooper;

    virtual ~FitRunner(){};


private:
    std::string output_file_name ;
    std::vector<std::string> alpha_names;
    std::map<std::string, std::string> hist_name_map;
    std::vector<double> data_nom_map;
    std::map<std::string, std::vector<double>> bkg_nom_map;
    std::map<std::string, std::vector<double>> bkg_err_map;
    std::map<std::string, std::vector<double>> sig_nom_map;
    std::map<std::string, std::vector<double>> sig_err_map;
    std::map<std::string, double> alpha_chi2;
    std::map<std::string, double> alpha_factor_Nom;
    std::map<std::string, double> alpha_factor_Err;
    double LearnRate ; 
    double StopEpsilon ; 
    double MomentumGamma;
    ConfigHolder config;

    void MakeChi2Plot(int degrees_of_freedom = 0){
        
        double yRange = 100. ;
        //TGraphErrors * points = new TGraphErrors();
        TGraph * points = new TGraph();
        
        int i_point = 0;
        //int un_i_point = 0;
        //int i_end = 300;

        //auto alpha_best_in_map = std::min_element( std::begin(this->alpha_chi2), std::end(this->alpha_chi2), 
        //        [](const decltype(this->alpha_chi2)::value_type& l, const decltype(this->alpha_chi2)::value_type& r){return l.second < r.second ; } );
        //
        //auto map_index = std::find(this->alpha_names.begin(), this->alpha_names.end(), alpha_best_in_map->first);
        //
        //if ( map_index != this->alpha_names.end() ){
        //    i_point = (map_index - this->alpha_names.begin() > i_end) ? -1*i_end : -1*(map_index - this->alpha_names.begin()) ;
        //    i_end   = (this->alpha_names.end() - map_index -1 > i_end) ? i_end : (this->alpha_names.end() - map_index - 1) ;
        //    int i_ = 0 ;
        //    while ( i_point <= i_end ){
        //        int index = map_index - this->alpha_names.begin() + i_point ;
        //        double alpha_num = this->str_to_value( alpha_names.at(index) );
        //        points->SetPoint(i_, alpha_num, this->alpha_chi2[alpha_names.at(index)]);
        //        i_point ++ ;
        //        i_ ++ ;
        //    }
        //} else {
        //    return;
        //    //LOG(Error);
        //}
        
        for(auto each_alpha : this->alpha_names){
            double alpha_num = this->str_to_value(each_alpha);
            if( this->alpha_chi2[each_alpha] < yRange ){
                points->SetPoint(i_point, alpha_num, this->alpha_chi2[each_alpha]);
                //points->SetPointError(i_point, 0. , 1. );
                i_point ++ ;
            }
        }
        
        TF1 *f_barabola = new TF1("f_barabola", "[0]+[1]*x+[2]*x*x", -10, 10);
        f_barabola->SetParameters(1, 1, 1 );
        f_barabola->SetLineColor(kBlue);
        
        TF1 *f_cubic = new TF1("f_cubic", "[0]+[1]*x+[2]*x*x+[3]*x*x*x", -10, 10);
        f_cubic->SetParameters(1, 1, 1, 1);
        f_cubic->SetLineColor(8); //Green
        
        //points->Fit("f_barabola");
        //points->Fit("f_cubic", "+" );
        TFitResultPtr FitResult_barabola = points->Fit("f_barabola","S");
        TFitResultPtr FitResult_cubic = points->Fit("f_cubic", "S+" );

        double chi2_barabola = FitResult_barabola->Chi2();
        double chi2_cubic = FitResult_cubic->Chi2();
        int ndf_barabola = FitResult_barabola->Ndf();
        int ndf_cubic = FitResult_cubic->Ndf();

        TCanvas * canvas = new TCanvas("canvas", "canvas", 0, 0, 800, 600);
        TPad *upperPad = new TPad("upperPad", "upperPad", .001, .001, 1.0, 1.0);
        upperPad->SetTopMargin(0.03);
        upperPad->Draw();
        upperPad->cd();
        
        //un_points->SetMarkerStyle(20);
        //un_points->SetMarkerSize(1);
        //un_points->SetMarkerColor(kRed);
        //un_points->SetLineWidth(2);
        //un_points->Draw("P");
        //un_points->Draw("SAME");
        //un_points->GetXaxis()->SetTitle("#alpha [%]");
        //un_points->GetYaxis()->SetTitle("#chi^{2}");
        //un_points->GetYaxis()->SetRangeUser(0., 100.);
        
        
        points->SetMarkerStyle(20);
        points->SetMarkerSize(1);
        points->SetMarkerColor(kRed);
        points->SetLineWidth(2);
        points->Draw("APSAME");
        points->Draw("SAME");
        points->GetXaxis()->SetTitle("#alpha [%]");
        points->GetYaxis()->SetTitle("#chi^{2}");
        points->GetYaxis()->SetRangeUser(0., yRange );
        
        //
        double fit_chi2 = f_barabola->GetChisquare() ;
        double a = f_barabola->GetParameter(2), b = f_barabola->GetParameter(1), c = f_barabola->GetParameter(0);
        double a_err = f_barabola->GetParError(2), b_err = f_barabola->GetParError(1), c_err = f_barabola->GetParError(0);
        double alpha_best = -1 * b / (2.0 * a);
        double alpha_best_err = sqrt( a ) / a ;
        double fit_min = (4*a*c-b*b)/(4*a) ;
        double minimal_chi2_ndf = 0 ;
        if( degrees_of_freedom > 0 ){
             minimal_chi2_ndf = fit_min / degrees_of_freedom;
        } 
        
        TLine* line1 = new TLine(alpha_best-alpha_best_err, 0 , alpha_best-alpha_best_err, fit_min+1);
        line1->SetLineWidth(3);
        line1->SetLineStyle(9);
        line1->Draw("same");
        TLine* line2 = new TLine(alpha_best+alpha_best_err, 0 , alpha_best+alpha_best_err, fit_min+1);
        line2->SetLineWidth(3);
        line2->SetLineStyle(9);
        line2->Draw("same");
        double fit_chi2_3 = f_cubic->GetChisquare() ;
        double a_3 = f_cubic->GetParameter(3)   , b_3 = f_cubic->GetParameter(2)   , c_3 = f_cubic->GetParameter(1)   , d_3 = f_cubic->GetParameter(0)   ;
        double a_err_3 = f_cubic->GetParError(3), b_err_3 = f_cubic->GetParError(2), c_err_3 = f_cubic->GetParError(1), d_err_3 = f_cubic->GetParError(0);
        double Delta_3 = sqrt(b_3*b_3-3*a_3*c_3) ;
        double alpha_best_3 = ( -1 * b_3 + Delta_3 ) / (3*a_3) ;
        double alpha_best_err_3 = sqrt( pow( (alpha_best_3/a_3+c_3/(2*a_3*Delta_3))*a_err_3 ,2) + pow( (alpha_best_3/Delta_3)*b_err_3 ,2) + pow( (1/(2*Delta_3))*c_err_3 ,2) );
        //
        
        TLatex l3(0.13, 0.90,"#bf{#it{ATLAS}} Internal");                                                                                                                                    
        l3.SetNDC(kTRUE);
        l3.SetTextSize(0.04);
        l3.SetTextFont(42);
        l3.Draw("same");
        
        TLatex l4(0.13,0.85,"#sqrt{s} = 13 TeV, 139 fb^{-1}");
        l4.SetNDC(kTRUE);
        l4.SetTextSize(0.03);
        l4.SetTextFont(42);
        l4.Draw("same");
        
        TLatex l7(0.13,0.8,(" #chi^2 / Nbins = "+Utils::to_string_with_precision(fit_min,2)+"/"+Utils::to_string_with_precision(degrees_of_freedom,2)+" = "+Utils::to_string_with_precision(minimal_chi2_ndf,2) ).c_str() );
        l7.SetNDC(kTRUE);
        l7.SetTextSize(0.03);
        l7.SetTextFont(42);
        l7.Draw("same");
        
        TLatex l5(0.13,0.75,(this->output_file_name+", #alpha = "+Utils::to_string_with_precision(alpha_best,2)+"%"+" #pm "+Utils::to_string_with_precision(alpha_best_err,2)+"%" 
                    + " #chi^2/NDF = "+Utils::to_string_with_precision(chi2_barabola/ndf_barabola,1)).c_str() );
        l5.SetNDC(kTRUE);
        l5.SetTextSize(0.03);
        l5.SetTextFont(42);
        l5.SetTextColor(kBlue);
        l5.Draw("same");
        
        TLatex l6(0.13,0.7,(this->output_file_name+", #alpha = "+Utils::to_string_with_precision(alpha_best_3,2)+"%"+" #pm "+Utils::to_string_with_precision(alpha_best_err_3,2)+"%"
                    + " #chi^2/NDF ="+Utils::to_string_with_precision(chi2_cubic/ndf_cubic,1)).c_str() );
        l6.SetNDC(kTRUE);
        l6.SetTextSize(0.03);
        l6.SetTextFont(42);
        l6.SetTextColor(8);
        l6.Draw("same");
       

        canvas->SaveAs( (this->output_file_name + ".png").c_str() );
        canvas->SaveAs( (this->output_file_name + ".eps").c_str() );
        
        //////////////////////////////////////////
        // Storing the fit results
        //////////////////////////////////////////
        std::ofstream fout(this->output_file_name + ".txt"); 
        //
        fout << "alpha_best: " << alpha_best << " $\\pm$ " << alpha_best_err << std::endl;
        //fout << "alpha_best_3: " << alpha_best_3 << " $\\pm$ " << alpha_best_err_3 << std::endl;
        
        std::string alpha_best_tmp = std::to_string( alpha_best * 0.01 ) ;
        std::string alpha_best_tmp2 = std::to_string( alpha_best * 0.01 ) ;
        if( alpha_best < 0 ){
            alpha_best_tmp = "m0p" + alpha_best_tmp.substr(3,3) + "0" ;
        } else {
            alpha_best_tmp = "0p" + alpha_best_tmp.substr(2,3) + "0" ;
        }
        fout << alpha_best_tmp << ", " << alpha_factor_Nom[alpha_best_tmp] << std::endl;

        for(auto each_alpha : this->alpha_names){
            fout<<"*alpha="<<each_alpha<<" chi2="<<alpha_chi2[each_alpha]<<" factor="<<alpha_factor_Nom[each_alpha]<<" factorErr="<<alpha_factor_Err[each_alpha]<<std::endl;
        }
        
        std::ofstream summary("SummaryAlpha.csv", std::ofstream::app); 
        summary << this->output_file_name << "," << alpha_best << "," << alpha_best_err << "," << minimal_chi2_ndf << "," << chi2_barabola << "," << ndf_barabola << "," << chi2_barabola/ndf_barabola << "," << minimal_chi2_ndf << "," << fit_min << ","<< degrees_of_freedom << "," << config.getBins() << std::endl;
    }
   

    void StoreAlpha(std::vector<std::string> alpha_names){ this->alpha_names = alpha_names; }
    void StoreParameter(double LR, double StopEps, double Momentum){ this->LearnRate = LR ; this->StopEpsilon = StopEps ; this->MomentumGamma = Momentum ; }
    void PrintOut(std::string chainname){ std::cout<<"*alpha="<<chainname<<" chi2="<<alpha_chi2[chainname]<<" factor="<<alpha_factor_Nom[chainname]<<" factorErr="<<alpha_factor_Err[chainname]<<std::endl; }


    std::pair<std::vector<double>,std::vector<double> > ReadHist(std::string RootFilePath , std::string RelHistPath){
        std::vector<double> Nom;
        std::vector<double> Err;
        TFile* tfile = new TFile(RootFilePath.c_str());
        TH1* hist = (TH1*)tfile->Get(RelHistPath.c_str());
        TH1* hist_new = nullptr ;
        if (config.checkRebins() > 0){
            hist_new = hist->Rebin(config.checkRebins()-1,"",config.getRebins());
        } else {
            hist_new = hist ;
        }
        int lastBinNum = hist_new->GetNbinsX();
        int firstBinNum = 1 ;
        for (int i = firstBinNum; i <= lastBinNum; i++) {
            Nom.push_back( hist_new->GetBinContent(i) );
            Err.push_back( hist_new->GetBinError(i) );
        }
        tfile->Close();
        return std::make_pair(Nom,Err);
    }
    

    std::pair<std::vector<double>,std::vector<double> > ReadHist(TH1F* hist){
        std::vector<double> Nom;
        std::vector<double> Err;
        TH1F *hnew = nullptr ;
        if (config.checkRebins() > 0){
            hnew = (TH1F*)hist->Rebin(config.checkRebins()-1,"",config.getRebins());
        } else {
            hnew = hist;
        }
        int lastBinNum = hnew->GetNbinsX();
        int firstBinNum = 1 ;
        for (int i = firstBinNum; i <= lastBinNum; i++) {
            Nom.push_back( hnew->GetBinContent(i) );
            Err.push_back( hnew->GetBinError(i) );
        }
        return std::make_pair(Nom,Err);
    }
    

    TH1* LoadHist(std::string RootFilePath , std::string RelHistPath){
        size_t pos = RootFilePath.find(".root:");
        if (pos != std::string::npos) {
            std::string root_file_name = RootFilePath.substr(0, pos + 6 - 1);
            std::string hist_prefix = RootFilePath.substr(pos + 6 );
            TFile* tfile = new TFile( root_file_name.c_str());
            TH1* hist = (TH1*)tfile->Get( (hist_prefix+RelHistPath).c_str());
            hist->Sumw2();
            return hist;
        }
        else{
            TFile* tfile = new TFile(RootFilePath.c_str());
            TH1* hist = (TH1*)tfile->Get(RelHistPath.c_str());
            hist->Sumw2();
            return hist;
        }
        //TFile* tfile = new TFile(RootFilePath.c_str());
        //TH1* hist = (TH1*)tfile->Get(RelHistPath.c_str());
        //hist->Sumw2();
        //return hist;
    }


    void ReadData(std::map<std::string, std::vector<std::string> > folder_path, std::string HistName, std::vector<std::string> activePeriod){
        TH1F* histData = nullptr ;
        for ( auto period : activePeriod ){
            for ( auto& i : folder_path[period] ){
                if ( histData == nullptr ) { histData = (TH1F*)LoadHist(i, HistName); }
                else{ histData->Add( (TH1F*)LoadHist(i, HistName ) ); }
            }
        }
        auto DataPair = ReadHist(histData);
        this->data_nom_map = DataPair.first;
    }
    
    //void ReadBkg(){
    //    TH1F* histWt    = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/Wt_0p0000.root", this->hist_name_map["BKG"]);
    //    TH1F* histWl    = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/Wl_0p0000.root", this->hist_name_map["BKG"]);
	//    TH1F* histZll   = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/Zll_0p0000.root", this->hist_name_map["BKG"]);
	//    TH1F* histTTbar = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/TTbar_0p0000.root", this->hist_name_map["BKG"]);
	//    TH1F* histST    = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/SingleTop_0p0000.root", this->hist_name_map["BKG"]);
	//    TH1F* histMB    = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/MultiBoson_0p0000.root", this->hist_name_map["BKG"]);
    //    
    //    TH1F* histQCD       = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/QCD_Data_0p0.root"      , this->hist_name_map["DATA"]);
    //    TH1F* histWt_QCD    = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/QCD_Wt_0p0.root"        , this->hist_name_map["QCD"]);
    //    TH1F* histWl_QCD    = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/QCD_Wl_0p0.root"        , this->hist_name_map["QCD"]);
	//    TH1F* histZll_QCD   = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/QCD_Zll_0p0.root"       , this->hist_name_map["QCD"]);
	//    TH1F* histZtt_QCD   = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/QCD_Ztt_0p0.root"       , this->hist_name_map["QCD"]);
	//    TH1F* histTTbar_QCD = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/QCD_TTbar_0p0.root"     , this->hist_name_map["QCD"]);
	//    TH1F* histST_QCD    = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/QCD_SingleTop_0p0.root" , this->hist_name_map["QCD"]);
	//    TH1F* histMB_QCD    = (TH1F*)LoadHist("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis_v04_New/WorkSpace_Medium/run/QCD_MultiBoson_0p0.root", this->hist_name_map["QCD"]);

    //    double k_w_1p_os = 1.01145 ;
    //    double k_w_1p_ss = 1.02039 ;
    //    double k_w_3p_os = 1.00752 ;
    //    double k_w_3p_ss = 1.04975 ;
    //    double r_1p = 1.19717 ;
    //    double r_3p = 1.36102 ;

    //    double k_w_os = 0. ;
    //    double k_w_ss = 0. ;
    //    double r_qcd = 0. ;

    //    if ( this->hist_name_map["DATA"].find("prong_1_") != std::string::npos ){
    //        r_qcd = r_1p ;
    //        k_w_os = k_w_1p_os ;
    //        k_w_ss = k_w_1p_ss ;
    //    } else if ( this->hist_name_map["DATA"].find("prong_3_") != std::string::npos ) {
    //        r_qcd = r_3p ;
    //        k_w_os = k_w_3p_os ;
    //        k_w_ss = k_w_3p_ss ;
    //    }
    //    
    //    histWl->Scale(k_w_os);
    //    histWt->Scale(k_w_os);
    //    
    //    histWl_QCD->Scale(k_w_ss);
    //    histWt_QCD->Scale(k_w_ss);
    //    
    //    histQCD->Add(histWt_QCD   , -1);
    //    histQCD->Add(histWl_QCD   , -1);
	//    histQCD->Add(histZll_QCD  , -1);
	//    histQCD->Add(histZtt_QCD  , -1);
	//    histQCD->Add(histTTbar_QCD, -1);
	//    histQCD->Add(histST_QCD   , -1);
	//    histQCD->Add(histMB_QCD   , -1);
    //    histQCD->Scale(r_qcd) ;

	//    histWt->Add(histWl       );
	//    histWt->Add(histZll      );
	//    histWt->Add(histTTbar    );
	//    histWt->Add(histST       );
	//    histWt->Add(histMB       );
    //    histWt->Add(histQCD      );
    //    
    //    auto BkgPair = ReadHist(histWt);
    //    this->bkg_nom_map["0p0000"] = BkgPair.first;
    //    this->bkg_err_map["0p0000"] = BkgPair.second;
    //    
    //}

    double str_to_value ( std::string each_alpha){
        size_t pos;
        std::string alpha = each_alpha;
        if ( (pos = alpha.find("p")) != std::string::npos ){
            alpha.replace(pos,1,".");
        }
        if ( alpha.find("m") != std::string::npos ){
            alpha.replace(0, 1, "-");
        }
        double alpha_num = 100 * std::stod(alpha);
        return alpha_num ;
    }

};

