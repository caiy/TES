#pragma once
#include <string>

class ConfigHolder {
public:

    ConfigHolder() {
    };

    ConfigHolder( std::map<std::string, std::string> hist_name_map, std::vector<std::string> alpha_collection ) {
        this->hist_name_map = hist_name_map ;
        this->alpha_collection = alpha_collection ;
    }

    void LoadRebins( std::string rebins ){
        std::stringstream ss(rebins);
        
        double tmpVal;
        while (ss >> tmpVal) {
            this->Rebins.push_back(tmpVal);
        }
    }

    int checkRebins(){
        if (  this->Rebins.size() > 0 ){
            return this->Rebins.size() ;
        } else {
            return 0;
        }
    }
    
    std::string getBins(){
        if (  this->Rebins.size() > 0 ){
            std::string rr = "";
            for(auto i : this->Rebins){
                rr =  rr + std::to_string(i).substr(0,2) + " " ;
            }
            return rr ;
        } else {
            return "0";
        }
    }
    
    double* getRebins(){
        return this->Rebins.data() ;
    }

    void addPairsToBKG(std::string file_path , std::string period){
        this->file_map[period].push_back( file_path );
    }
    
    void addPairsToQCD(std::string file_path , std::string period){
        this->qcd_file_map[period].push_back( file_path );
    }
    
    void addPairsToData(std::string file_path , std::string period){ 
        this->data_file[period].push_back( file_path ) ; 
    }
    void addPairsToSIG(std::string file_path , std::string period){ 
        this->sig_file[period].push_back( file_path ) ; 
    }
    
    void addPeriod(std::string period ){
        this->ActivePeriod.push_back(period);
        std::vector<std::string> temp;
        this->qcd_file_map[period] = temp;
        this->file_map[period] = temp;
        this->data_file[period] = temp;
        this->sig_file[period] = temp;
    }

    std::map<std::string, std::vector<std::string> > getQCD (){ return this->qcd_file_map;  }
    std::map<std::string, std::vector<std::string> > getOther (){     return this->file_map;      }
    std::map<std::string, std::vector<std::string> > getData (){ return this->data_file ; }
    std::map<std::string, std::vector<std::string> > getSignal (){ return this->sig_file ; }
    
    std::map<std::string, std::string> hist_name_map;
    std::vector<std::string> alpha_collection;

    std::map<std::string, double > k_w_1p_os ;
    std::map<std::string, double > k_w_1p_ss ;
    std::map<std::string, double > k_w_3p_os ;
    std::map<std::string, double > k_w_3p_ss ;
    std::map<std::string, double > r_1p ;
    std::map<std::string, double > r_3p ;
    
    double LearnRate ;
    double Stop ;
    double MomentumGamma ;

    std::vector<std::string> ActivePeriod ;

private:
    std::string output_file_name ;
    std::vector<double> Rebins;
    std::map<std::string, std::vector<std::string> > qcd_file_map;
    std::map<std::string, std::vector<std::string> > file_map;
    std::map<std::string, std::vector<std::string> > data_file;
    std::map<std::string, std::vector<std::string> > sig_file;
};

