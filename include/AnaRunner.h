#include <functional>
#include <map>
#include <string>
#include <vector>

#include "ConfigHolder.h"

/* The interface for how to loop the trees. This is the interface class of all kinds of Analysis runners.
 * There should not be any non-virtual funcs in this class to avoid possiable issues in multiple inheritance */
class AnaRunner {
public:
    virtual void runChains(ConfigHolder config) = 0;
    virtual ~AnaRunner(){};
};

/********************************************************************************************************
 * Used for define a new analysis! You could define your analysis using DefineAnalysis(YouAnalysisName).
 * Then it will be registered in the analysis list! Thanks the simpleAnalysis for this great idea!
 * Differences between this version and the simpleAnalysis: In my version, the analysis
 * 1. Could run only one analysis in a run.
 * 2. Has slightly more memory waste.
 * They are due to in simpleAnalysis, for each analysis there will be only one runner which is the one registered
 * here to run analysis. But in my version we have multiple analysis runners for multiple thread support.
 *********************************************************************************************************/
// declarations
std::vector<std::string> getAnaList();
AnaRunner* getNewAnaRunner(std::string analysisName);
std::map<std::string, std::function<AnaRunner*()>>& getAnaCollection();
