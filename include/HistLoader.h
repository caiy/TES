#pragma once

#include <algorithm>
#include <functional>
#include <memory>
#include <string>
#include <vector>
#include <cmath>

#include "Runnable.h"
#include "ConfigHolder.h"

class HistLoader : public Runnable {
public:

    HistLoader() {
    };
    
    HistLoader(std::string chainName, ConfigHolder config, std::vector<double>* SigNom, std::vector<double>* SigErr ){
        setAnaChain(chainName, config, SigNom, SigErr) ;
    }

    HistLoader(std::string chainName, ConfigHolder config, std::vector<double>* SigNom, std::vector<double>* SigErr, std::vector<double>* BkgNom, std::vector<double>* BkgErr ){
        setAnaChain(chainName, config, BkgNom, BkgErr, SigNom, SigErr) ;
    }

    void setAnaChain(std::string chainName, ConfigHolder config, std::vector<double>* SigNom, std::vector<double>* SigErr ) {
        // lock area
        std::lock_guard<std::mutex> lock{_lock};
        this->alpha_name = chainName;
        this->config = config;
        this->hist_name_map = config.hist_name_map;
        this->sig_yields = SigNom;
        this->sig_error = SigErr;
    }
    
    void setAnaChain(std::string chainName, ConfigHolder config, std::vector<double>* BkgNom, std::vector<double>* BkgErr, std::vector<double>* SigNom, std::vector<double>* SigErr ) {
        // lock area
        std::lock_guard<std::mutex> lock{_lock};
        this->alpha_name = chainName;
        this->config = config;
        this->hist_name_map = this->config.hist_name_map;
        this->bkg_yields = BkgNom;
        this->bkg_error = BkgErr;
        this->sig_yields = SigNom;
        this->sig_error = SigErr;
    }
    
    void run() override {
        ReadBkg();
        ReadSig();
    }

    void ReadBkg(){
        auto k_w_1p_os = this->config.k_w_1p_os ;
        auto k_w_1p_ss = this->config.k_w_1p_ss ;
        auto k_w_3p_os = this->config.k_w_3p_os ;
        auto k_w_3p_ss = this->config.k_w_3p_ss ;
        auto r_1p =      this->config.r_1p      ;
        auto r_3p =      this->config.r_3p      ;
        
        std::map<std::string, double > k_w_os ;
        std::map<std::string, double > k_w_ss ;
        std::map<std::string, double > r_qcd  ;
       

        if ( this->hist_name_map["DATA"].find("prong_1_") != std::string::npos ){
            r_qcd = r_1p ;
            k_w_os = k_w_1p_os ;
            k_w_ss = k_w_1p_ss ;
        } else if ( this->hist_name_map["DATA"].find("prong_3_") != std::string::npos ) {
            r_qcd = r_3p ;
            k_w_os = k_w_3p_os ;
            k_w_ss = k_w_3p_ss ;
        } else if ( this->hist_name_map["DATA"].find("1p") != std::string::npos ){
            r_qcd = r_1p ;
            k_w_os = k_w_1p_os ;
            k_w_ss = k_w_1p_ss ;
        } else if ( this->hist_name_map["DATA"].find("3p") != std::string::npos ) {
            r_qcd = r_3p ;
            k_w_os = k_w_3p_os ;
            k_w_ss = k_w_3p_ss ;
        }
        
        
        TH1F * histBKG_other = nullptr ;
        TH1F * histW = nullptr ;
        for ( auto period : this->config.ActivePeriod ){
            //std::cout<<period<<std::endl;
            auto x = this->config.getOther();
            for ( auto& i : x[period] ){
                std::cout<<"reading(BKG): "<<period<<" "<<i<<std::endl;
                if ( histBKG_other == nullptr ) { histBKG_other = (TH1F*)LoadHist(i+this->alpha_name, this->hist_name_map["BKG"]) ; }
                else{ histBKG_other->Add( (TH1F*)LoadHist(i+this->alpha_name, this->hist_name_map["BKG"]) ); }
            }
            
            //x = this->config.getW();
            //for ( auto& i : x[period] ){
            //    if ( histW == nullptr ) { 
            //        histW = (TH1F*)LoadHist(i+this->alpha_name+".root", this->hist_name_map["BKG"]) ; 
            //        histW->Scale(k_w_os[period]);
            //    } else { 
            //        auto hist_tmp = (TH1F*)LoadHist(i+this->alpha_name+".root", this->hist_name_map["BKG"]) ;
            //        hist_tmp->Scale(k_w_os[period]);
            //        histW->Add( hist_tmp ); 
            //    }
            //}
	    }
        //histW->Add(histBKG_other);
        histW = histBKG_other;
            
        for ( auto period : this->config.ActivePeriod ){
            TH1F * histQCD = nullptr ;
            auto x = this->config.getQCD();
            for ( auto& i : x[period] ){
                std::cout<<"reading(QCD): "<<period<<" "<<i<<std::endl;
                if ( histQCD == nullptr ) { histQCD = (TH1F*)LoadHist(i, this->hist_name_map["QCD"]) ; }
                else{ histQCD->Add( (TH1F*)LoadHist(i, this->hist_name_map["QCD"]) ); }
            }
            
            //histQCD->Scale(r_qcd[period]) ;
            histW->Add(histQCD      );
        }
        auto BkgPair = ReadHist(histW);
        *this->bkg_yields = BkgPair.first;
        *this->bkg_error = BkgPair.second;
    }
    
    void ReadSig(){
        TH1F* histSig = nullptr ;
        for ( auto period : this->config.ActivePeriod ){
            auto x = this->config.getSignal();
            for ( auto& i : x[period] ){
                std::cout<<"reading(SIG): "<<period<<" "<<i<<std::endl;
                if ( histSig == nullptr ) { histSig = (TH1F*)LoadHist(i+this->alpha_name, this->hist_name_map["SIG"]) ; }
                else{ histSig->Add( (TH1F*)LoadHist(i+this->alpha_name, this->hist_name_map["SIG"]) ); }
            }
        }
        auto SigPair = ReadHist(histSig);
        *this->sig_yields = SigPair.first;
        *this->sig_error = SigPair.second;
        //for (int i = 0; i < this->sig_error->size(); i++) {
        //    this->sig_error->at(i) *= 0.58 ;
        //}
        for (int i = 0; i < this->sig_error->size(); i++) {
            std::cout<<this->sig_error->at(i)/this->sig_yields->at(i)<<" ";
        }
        std::cout<<std::endl;
    }


    virtual ~HistLoader() {
        std::lock_guard<std::mutex> lock{_lock};
        //std::cout << "end of " << this->alpha_name << std::endl;
        // delete chain
        //SafeDelete(anaChain);
        // write and delete alloc trees
    }

private:
    std::mutex _lock; // for asyn
    std::string alpha_name;
    ConfigHolder config;
    std::map<std::string,std::string> hist_name_map;

    std::vector<double>* bkg_yields;
    std::vector<double>* bkg_error;
    std::vector<double>* sig_yields;
    std::vector<double>* sig_error;
    
    
    std::pair<std::vector<double>,std::vector<double> > ReadHist(std::string RootFilePath , std::string histname){
        std::vector<double> Nom;
        std::vector<double> Err;
        TFile* tfile = new TFile(RootFilePath.c_str());
        TH1* hist = (TH1*)tfile->Get(histname.c_str());
        TH1* hist_new = nullptr ;
        if (config.checkRebins() > 0){
            hist_new = hist->Rebin(config.checkRebins()-1,"",config.getRebins());
        } else {
            hist_new = hist ;
        }
        int lastBinNum = hist_new->GetNbinsX();
        int firstBinNum = 1 ;
        for (int i = firstBinNum; i <= lastBinNum; i++) {
            Nom.push_back( hist_new->GetBinContent(i) );
            Err.push_back( hist_new->GetBinError(i) );
        }
        tfile->Close();
        return std::make_pair(Nom,Err);
    }
    
    std::pair<std::vector<double>,std::vector<double> > ReadHist(TH1F* hist){
        std::vector<double> Nom;
        std::vector<double> Err;
        TH1F *hnew = nullptr;
        if (config.checkRebins() > 0){
            hnew = (TH1F*)hist->Rebin(config.checkRebins()-1,"",config.getRebins());
        } else {
            hnew = hist;
        }
        int lastBinNum = hnew->GetNbinsX();
        int firstBinNum = 1 ;
        for (int i = firstBinNum; i <= lastBinNum; i++) {
            Nom.push_back( hnew->GetBinContent(i) );
            Err.push_back( hnew->GetBinError(i) );
        }
        return std::make_pair(Nom,Err);
    }
    
    TH1* LoadHist(std::string RootFilePath , std::string histname){
        size_t pos = RootFilePath.find(".root:");
        if (pos != std::string::npos) {
            std::string root_file_name = RootFilePath.substr(0, pos + 6 - 1);
            std::string hist_prefix = RootFilePath.substr(pos + 6 );
            TFile* tfile = new TFile( root_file_name.c_str());
            TH1* hist = (TH1*)tfile->Get( (hist_prefix+histname).c_str());
            hist->Sumw2();
            return hist;
        }
        else{
            TFile* tfile = new TFile(RootFilePath.c_str());
            TH1* hist = (TH1*)tfile->Get(histname.c_str());
            hist->Sumw2();
            return hist;
        }
    }
    
    TH1* LoadHist(std::string RootFilePath , std::string histname, std::string alphaname){
        TFile* tfile = new TFile(RootFilePath.c_str());
        TH1* hist = (TH1*)tfile->Get(histname.c_str());
        hist->Sumw2();
        return hist;
    }
};
