all_selected=('prong_1_AllRegion' 'prong_1_CentralRegion' 'prong_1_ForwardRegion' 'prong_3_AllRegion' 'prong_3_CentralRegion' 'prong_3_ForwardRegion')


for selected in "${all_selected[@]}"
do
    xxx=$selected
    sig=${xxx}"/NOMINAL"
    
    
    mkdir run_${xxx}
    cd run_${xxx}/ && rm -rf *
    #cd run_${xxx}/ 
    cp ../../build/mini_analysis ./
    
    Data="${xxx}/NOMINAL"
    QCD="${xxx}/Nominal"
    Bkg=(\
    "${xxx}/Nominal" \
    "${xxx}/lumi_UP" \
    "${xxx}/lumi_DOWN" \
    "${xxx}/jet_forward_JVT_UP" \
    "${xxx}/jet_forward_JVT_DOWN" \
    "${xxx}/jet_central_JVT_UP" \
    "${xxx}/jet_central_JVT_DOWN" \
    "${xxx}/jet_FT_EFF_B_UP" \
    "${xxx}/jet_FT_EFF_B_DOWN" \
    "${xxx}/jet_FT_EFF_C_UP" \
    "${xxx}/jet_FT_EFF_C_DOWN" \
    "${xxx}/jet_FT_EFF_Light_UP" \
    "${xxx}/jet_FT_EFF_Light_DOWN" \
    "${xxx}/jet_FT_EFF_extrapolation_from_charm_UP" \
    "${xxx}/jet_FT_EFF_extrapolation_from_charm_DOWN" \
    "${xxx}/lep_Reco_SYS_UP" \
    "${xxx}/lep_Reco_SYS_DOWN" \
    "${xxx}/lep_Reco_STAT_UP" \
    "${xxx}/lep_Reco_STAT_DOWN" \
    "${xxx}/lep_Iso_SYS_UP" \
    "${xxx}/lep_Iso_SYS_DOWN" \
    "${xxx}/lep_Iso_STAT_UP" \
    "${xxx}/lep_Iso_STAT_DOWN" \
    "${xxx}/TrigStat_UP" \
    "${xxx}/TrigStat_DOWN" \
    "${xxx}/TrigSyst_UP" \
    "${xxx}/TrigSyst_DOWN" \
    "${xxx}/tau_Reco_UP" \
    "${xxx}/tau_Reco_DOWN" \
    "${xxx}/tau_ID_2025_UP" \
    "${xxx}/tau_ID_2530_UP" \
    "${xxx}/tau_ID_3040_UP" \
    "${xxx}/tau_ID_GE40_UP" \
    "${xxx}/tau_ID_2025_DOWN" \
    "${xxx}/tau_ID_2530_DOWN" \
    "${xxx}/tau_ID_3040_DOWN" \
    "${xxx}/tau_ID_GE40_DOWN" \
    "${xxx}/tau_ID_Syst_UP" \
    "${xxx}/tau_ID_Syst_DOWN" \
    )
    Range='"42 48 54 60 66 72 78 84 90"'

    i=0
    
    for ii in "${Bkg[@]}"
    do
        echo  ./mini_analysis -d ${Data} -b ${ii} -q ${QCD} -r ${Range} -j ../config.json > sub.sh."$i"
        chmod u+x sub.sh."$i"
        let i++
    done
    
    hep_sub sub.sh."%{ProcId}" -g atlas -n "$i"
    cd ../
done
