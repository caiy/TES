import numpy as np
import csv
import matplotlib.pyplot as plt

def load_csv(file_path, file_name , key_name = "" ):
    nom = {}
    err = {}
    for each_path in file_path:
        with open(each_path+file_name, 'r' ) as csv_file:
            each_csv = csv.reader(csv_file, delimiter=',')
            for row in each_csv:
                nom[row[0]] = float(row[1])
                err[row[0]] = float(row[2])
    #print( nom, err )
    if key_name :
        for key in nom :
            if key_name == key :
                return nom[key_name]
    else:
        return nom, err

def AddAdditional(in_list, sys_name, value_up, value_dw ):
    in_list[sys_name+"_UP"] = value_up
    in_list[sys_name+"_DOWN"] = value_dw
    #return in_list

def CalSys(prong_and_region, nom_list, err_list):
    Sys_Alpha_Map = {}
    Sys_Stat_Map = {}
    NominalName = "Nominal"
    SysList = [\
        "Nominal",\
        "jet_central_JVT_DOWN",\
        "jet_central_JVT_UP",\
        "jet_forward_JVT_DOWN",\
        "jet_forward_JVT_UP",\
        "jet_FT_EFF_B_DOWN",\
        "jet_FT_EFF_B_UP",\
        "jet_FT_EFF_C_DOWN",\
        "jet_FT_EFF_C_UP",\
        "jet_FT_EFF_extrapolation_DOWN",\
        "jet_FT_EFF_extrapolation_from_charm_DOWN",\
        "jet_FT_EFF_extrapolation_from_charm_UP",\
        "jet_FT_EFF_extrapolation_UP",\
        "jet_FT_EFF_Light_DOWN",\
        "jet_FT_EFF_Light_UP",\
        "lep_Iso_STAT_DOWN",\
        "lep_Iso_STAT_UP",\
        "lep_Iso_SYS_DOWN",\
        "lep_Iso_SYS_UP",\
        "lep_Reco_STAT_DOWN",\
        "lep_Reco_STAT_UP",\
        "lep_Reco_SYS_DOWN",\
        "lep_Reco_SYS_UP",\
        "lumi_DOWN",\
        "lumi_UP",\
        "tau_eVeto_DOWN",\
        "tau_eVeto_UP",\
        "tau_ID_2025_DOWN",\
        "tau_ID_2530_DOWN",\
        "tau_ID_3040_DOWN",\
        "tau_ID_GE40_DOWN",\
        "tau_ID_Syst_DOWN",\
        "tau_ID_Syst_UP",\
        "tau_ID_2025_UP",\
        "tau_ID_2530_UP",\
        "tau_ID_3040_UP",\
        "tau_ID_GE40_UP",\
        "tau_Reco_DOWN",\
        "tau_Reco_UP",\
        "TrigStat_DOWN",\
        "TrigStat_UP",\
        "TrigSyst_DOWN",\
        "TrigSyst_UP",\
    ]
    
    SysList = [\
        "jet_central_JVT",\
        "jet_forward_JVT",\
        "jet_FT_EFF_B",\
        "jet_FT_EFF_C",\
        "jet_FT_EFF_extrapolation_from_charm",\
        "jet_FT_EFF_Light",\
        "lep_Iso_STAT",\
        "lep_Iso_SYS",\
        "lep_Reco_STAT",\
        "lep_Reco_SYS",\
        "lumi",\
        "tau_ID_2025",\
        "tau_ID_2530",\
        "tau_ID_3040",\
        "tau_ID_GE40",\
        "tau_ID_Syst",\
        "tau_Reco",\
        "TrigStat",\
        "TrigSyst",\
    ]
        #"jet_FT_EFF_extrapolation",\
        #"tau_eVeto",\

    AdditionalSyst=[\
        "BinWidth",\
        "FitRange",\
    ]

    Sys_DeltaAlpha_Map = {}
    alphaSys_value = 0

    for Sys in SysList :
        sys_up_value = nom_list[prong_and_region+"_"+Sys+"_UP"]
        sys_dw_value = nom_list[prong_and_region+"_"+Sys+"_DOWN"]
        sigma_up = ( sys_up_value - nom_list[prong_and_region+"_"+NominalName] ) / nom_list[prong_and_region+"_"+NominalName]
        sigma_dw = ( sys_dw_value - nom_list[prong_and_region+"_"+NominalName] ) / nom_list[prong_and_region+"_"+NominalName]
        #print(sigma_up,sigma_dw)
        if abs(sigma_up) > abs(sigma_dw) :
            Sys_DeltaAlpha_Map[Sys] = sigma_up
            alphaSys_value = np.sqrt( alphaSys_value*alphaSys_value + sigma_up*sigma_up )
        else:
            Sys_DeltaAlpha_Map[Sys] = sigma_dw
            alphaSys_value = np.sqrt( alphaSys_value*alphaSys_value + sigma_dw*sigma_dw )
    
    for Sys in AdditionalSyst :
        sys_up_value = nom_list[prong_and_region+"_"+Sys+"_UP"]
        sys_dw_value = nom_list[prong_and_region+"_"+Sys+"_DOWN"]
        sigma_up = ( sys_up_value - nom_list[prong_and_region+"_"+NominalName] ) / nom_list[prong_and_region+"_"+NominalName]
        sigma_dw = ( sys_dw_value - nom_list[prong_and_region+"_"+NominalName] ) / nom_list[prong_and_region+"_"+NominalName]
        #print(sigma_up,sigma_dw)
        if abs(sigma_up) > abs(sigma_dw) :
            Sys_DeltaAlpha_Map[Sys] = sigma_up
        else:
            Sys_DeltaAlpha_Map[Sys] = sigma_dw

    Sys_DeltaAlpha_Map["Stat"] = err_list[prong_and_region+"_"+NominalName] / nom_list[prong_and_region+"_"+NominalName]
    
    print(Sys_DeltaAlpha_Map,alphaSys_value)
    

    """ Begin plotting """
    xticks = []
    delta_alpha = []
    delta_alpha_err = []
    sorted_Sys_keys = sorted( Sys_DeltaAlpha_Map, key=lambda dict_key: abs(Sys_DeltaAlpha_Map[dict_key]) , reverse=True )
    #print(sorted_Sys_keys)
    
    for key in sorted_Sys_keys:
        xticks.append(key)
        delta_alpha.append(Sys_DeltaAlpha_Map[key]*nom_list[prong_and_region+"_"+NominalName])
        delta_alpha_err.append(abs(Sys_DeltaAlpha_Map[key]))

    #dump_csv = [round(nom_list[prong_and_region+"_"+NominalName],2), abs(round(nom_list[prong_and_region+"_"+NominalName]*alphaSys_value,2)), abs(round(err_list[prong_and_region+"_"+NominalName],2))]
    dump_csv = [ nom_list[prong_and_region+"_"+NominalName], abs(nom_list[prong_and_region+"_"+NominalName]*alphaSys_value), abs(err_list[prong_and_region+"_"+NominalName])]
    sum_begin = r"$\alpha_{Nom \pm Sys \pm Stat"
    sum_text = str(round(nom_list[prong_and_region+"_"+NominalName],2))+"%" + r"$\pm$"+str( abs(round(nom_list[prong_and_region+"_"+NominalName]*alphaSys_value,2)) )+"%" + r"$\pm$"+str( abs(round(err_list[prong_and_region+"_"+NominalName],2)) )+"%"
    
    if "BinWidth" in AdditionalSyst:
        sum_text = sum_text + r"$\pm$" + str( abs(round(nom_list[prong_and_region+"_"+NominalName]*Sys_DeltaAlpha_Map["BinWidth"],2)) ) + "%"
        sum_begin = sum_begin + r" \pm BinWidth"
        #dump_csv.append(abs(round(nom_list[prong_and_region+"_"+NominalName]*Sys_DeltaAlpha_Map["BinWidth"],2)))
        dump_csv.append(abs(nom_list[prong_and_region+"_"+NominalName]*Sys_DeltaAlpha_Map["BinWidth"]))
    if "FitRange" in AdditionalSyst:
        sum_text = sum_text + r"$\pm$" + str( abs(round(nom_list[prong_and_region+"_"+NominalName]*Sys_DeltaAlpha_Map["FitRange"],2)) ) + "%"
        sum_begin = sum_begin + r" \pm FitRange"
        #dump_csv.append(abs(round(nom_list[prong_and_region+"_"+NominalName]*Sys_DeltaAlpha_Map["FitRange"],2)))
        dump_csv.append(abs(nom_list[prong_and_region+"_"+NominalName]*Sys_DeltaAlpha_Map["FitRange"]))

    fig, ax = plt.subplots(1,1)
    ax.bar(range(len(xticks)), delta_alpha, align='center')
    ax.set_xticks(range(len(xticks)))
    ax.set_xticklabels(xticks, fontsize=6)
    plt.setp(ax.get_xticklabels(), rotation=30, ha="right", rotation_mode="anchor")

    ax.text(0.30, 0.93, sum_begin + r"}$= " + sum_text , fontsize=8, transform = ax.transAxes)
    ax.text(0.30, 0.86, prong_and_region , fontsize=8, transform = ax.transAxes)
    ax.text(0.30, 0.79, r"total relative $\sigma_{\alpha}=$"+str(round(100*alphaSys_value,1))+"%" , fontsize=8, transform = ax.transAxes)
    ax.text(0.30, 0.72, "relative Unc. : xx%", fontsize=8 , color = 'g', transform = ax.transAxes)
    
    ax.text(0.02, 0.92, r'ATLAS',fontsize=12, weight="bold",style="italic", transform = ax.transAxes)
    ax.text(0.15, 0.92, "Internal" , fontsize=12 , transform = ax.transAxes)
    
    ax.plot([-0.5,len(delta_alpha)-0.5],[0,0],linewidth=1,color='0.8',linestyle=(0,(1,1)) ) 
    for i in range(len(delta_alpha)):
        x_pos = i - 0.4
        if delta_alpha[i] != 0:
            y_pos = delta_alpha[i] + 0.06 * delta_alpha[i]/abs(delta_alpha[i]) - 0.03
        else:
            y_pos = delta_alpha[i] + 0.06 - 0.03
        ax.text(x_pos, y_pos, str(round(100*delta_alpha_err[i],1))+"%", color='g',fontsize=4)

    ax.set_ylim( [-1,1] )
    ax.set_ylabel(r"(Absolute) $\Delta\alpha$[%] " , fontsize=12)
    ax.set_xlabel("Systematic" , fontsize=12)
    
    fig.tight_layout()
    fig.savefig(prong_and_region+".png", dpi=600)
    #fig.savefig(prong_and_region+".eps")
    with open(prong_and_region+'.csv', 'a') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',')
        spamwriter.writerow(dump_csv)

#CalSys("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/TES/thread/run_prong_1_AllRegion/","prong_1_AllRegion_")
#CalSys("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/TES/thread/run_prong_3_AllRegion/","prong_3_AllRegion_")
#
#CalSys("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/TES/thread/run_prong_1_ForwardRegion/","prong_1_ForwardRegion_")
#CalSys("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/TES/thread/run_prong_3_ForwardRegion/","prong_3_ForwardRegion_")
#
#CalSys("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/TES/thread/run_prong_1_CentralRegion/","prong_1_CentralRegion_")
#CalSys("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/TES/thread/run_prong_3_CentralRegion/","prong_3_CentralRegion_")


file_path=[]
file_path.append("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeV/run_prong_1_AllRegion/"    )
file_path.append("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeV/run_prong_3_AllRegion/"    )
file_path.append("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeV/run_prong_1_ForwardRegion/")
file_path.append("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeV/run_prong_3_ForwardRegion/")
file_path.append("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeV/run_prong_1_CentralRegion/")
file_path.append("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeV/run_prong_3_CentralRegion/")

file_name="SummaryAlpha.csv"

nom,err = load_csv(file_path,file_name)

prong_1_A_2GeV = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/2GeV/run_prong_1_AllRegion/"    ],file_name,"prong_1_AllRegion_Nominal")
prong_3_A_2GeV = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/2GeV/run_prong_3_AllRegion/"    ],file_name,"prong_3_AllRegion_Nominal")
prong_1_F_2GeV = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/2GeV/run_prong_1_ForwardRegion/"],file_name,"prong_1_ForwardRegion_Nominal")
prong_3_F_2GeV = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/2GeV/run_prong_3_ForwardRegion/"],file_name,"prong_3_ForwardRegion_Nominal")
prong_1_C_2GeV = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/2GeV/run_prong_1_CentralRegion/"],file_name,"prong_1_CentralRegion_Nominal")
prong_3_C_2GeV = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/2GeV/run_prong_3_CentralRegion/"],file_name,"prong_3_CentralRegion_Nominal")

prong_1_A_6GeV = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/6GeV/run_prong_1_AllRegion/"    ],file_name,"prong_1_AllRegion_Nominal")
prong_3_A_6GeV = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/6GeV/run_prong_3_AllRegion/"    ],file_name,"prong_3_AllRegion_Nominal")
prong_1_F_6GeV = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/6GeV/run_prong_1_ForwardRegion/"],file_name,"prong_1_ForwardRegion_Nominal")
prong_3_F_6GeV = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/6GeV/run_prong_3_ForwardRegion/"],file_name,"prong_3_ForwardRegion_Nominal")
prong_1_C_6GeV = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/6GeV/run_prong_1_CentralRegion/"],file_name,"prong_1_CentralRegion_Nominal")
prong_3_C_6GeV = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/6GeV/run_prong_3_CentralRegion/"],file_name,"prong_3_CentralRegion_Nominal")

AddAdditional(nom, "prong_1_AllRegion"    +"_BinWidth", prong_1_A_2GeV, prong_1_A_6GeV)
AddAdditional(nom, "prong_3_AllRegion"    +"_BinWidth", prong_3_A_2GeV, prong_3_A_6GeV)
AddAdditional(nom, "prong_1_ForwardRegion"+"_BinWidth", prong_1_F_2GeV, prong_1_F_6GeV)
AddAdditional(nom, "prong_3_ForwardRegion"+"_BinWidth", prong_3_F_2GeV, prong_3_F_6GeV)
AddAdditional(nom, "prong_1_CentralRegion"+"_BinWidth", prong_1_C_2GeV, prong_1_C_6GeV)
AddAdditional(nom, "prong_3_CentralRegion"+"_BinWidth", prong_3_C_2GeV, prong_3_C_6GeV)
#print(nom)
prong_1_A_L = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeVLeft/run_prong_1_AllRegion/"    ],file_name,"prong_1_AllRegion_Nominal")
prong_3_A_L = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeVLeft/run_prong_3_AllRegion/"    ],file_name,"prong_3_AllRegion_Nominal")
prong_1_F_L = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeVLeft/run_prong_1_ForwardRegion/"],file_name,"prong_1_ForwardRegion_Nominal")
prong_3_F_L = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeVLeft/run_prong_3_ForwardRegion/"],file_name,"prong_3_ForwardRegion_Nominal")
prong_1_C_L = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeVLeft/run_prong_1_CentralRegion/"],file_name,"prong_1_CentralRegion_Nominal")
prong_3_C_L = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeVLeft/run_prong_3_CentralRegion/"],file_name,"prong_3_CentralRegion_Nominal")

prong_1_A_R = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeVRight/run_prong_1_AllRegion/"    ],file_name,"prong_1_AllRegion_Nominal")
prong_3_A_R = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeVRight/run_prong_3_AllRegion/"    ],file_name,"prong_3_AllRegion_Nominal")
prong_1_F_R = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeVRight/run_prong_1_ForwardRegion/"],file_name,"prong_1_ForwardRegion_Nominal")
prong_3_F_R = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeVRight/run_prong_3_ForwardRegion/"],file_name,"prong_3_ForwardRegion_Nominal")
prong_1_C_R = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeVRight/run_prong_1_CentralRegion/"],file_name,"prong_1_CentralRegion_Nominal")
prong_3_C_R = load_csv(["/publicfs/atlas/atlasnew/SUSY/users/caiyc/R22_tau/TES_New/4GeVRight/run_prong_3_CentralRegion/"],file_name,"prong_3_CentralRegion_Nominal")

AddAdditional(nom, "prong_1_AllRegion"    +"_FitRange", prong_1_A_L, prong_1_A_R )
AddAdditional(nom, "prong_3_AllRegion"    +"_FitRange", prong_3_A_L, prong_3_A_R )
AddAdditional(nom, "prong_1_ForwardRegion"+"_FitRange", prong_1_F_L, prong_1_F_R )
AddAdditional(nom, "prong_3_ForwardRegion"+"_FitRange", prong_3_F_L, prong_3_F_R )
AddAdditional(nom, "prong_1_CentralRegion"+"_FitRange", prong_1_C_L, prong_1_C_R )
AddAdditional(nom, "prong_3_CentralRegion"+"_FitRange", prong_3_C_L, prong_3_C_R )

CalSys("prong_1_AllRegion", nom, err)
CalSys("prong_3_AllRegion", nom, err)
CalSys("prong_1_ForwardRegion", nom, err)
CalSys("prong_3_ForwardRegion", nom, err)
CalSys("prong_1_CentralRegion", nom, err)
CalSys("prong_3_CentralRegion", nom, err)
