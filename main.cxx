#include <iostream>
#include <boost/program_options.hpp>
#include <mutex>
#include <thread>
#include <nlohmann/json.hpp>

#include "FitRunner.h"
#include "ConfigHolder.h"

using json = nlohmann::json;

//std::thread::id main_thread_id = std::this_thread::get_id();
//
//void hello()  
//{
//    std::cout << "Hello Concurrent World\n";
//    if (main_thread_id == std::this_thread::get_id())
//        std::cout << "This is the main thread.\n";
//    else
//        std::cout << "This is not the main thread.\n";
//}
//
//void pause_thread(int n) {
//    std::this_thread::sleep_for(std::chrono::seconds(n));
//    std::cout << "pause of " << n << " seconds ended\n";
//}

int main(int argc, char* argv[] = NULL) {
//    std::thread t(hello);
//    std::cout << t.hardware_concurrency() << std::endl;//可以并发执行多少个(不准确)
//    std::cout << "native_handle " << t.native_handle() << std::endl;//可以并发执行多少个(不准确)
//    t.join();
//    std::thread a(hello);
//    a.detach();
//    std::thread threads[5];                         // 默认构造线程
//
//    std::cout << "Spawning 5 threads...\n";
//    for (int i = 0; i < 5; ++i)
//        threads[i] = std::thread(pause_thread, i + 1);   // move-assign threads
//    std::cout << "Done spawning threads. Now waiting for them to join:\n";
//    for (auto &thread : threads)
//        thread.join();
//    std::cout << "All threads joined!\n";
    namespace po = boost::program_options;
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("JsonPath,j", po::value<std::string>(), "path of json file")
        ("Path,p", po::value<std::string>(), "path of root file")
        ("SIG,s", po::value<std::string>(), "hist name of signal file")
        ("DATA,d", po::value<std::string>(), "hist name of data file")
        ("BKG,b", po::value<std::string>(), "hist name of bkg file")
        ("QCD,q", po::value<std::string>(), "hist name of qcd file")
        ("Output,o", po::value<std::string>(), "out name")
        ("Rebin,r", po::value<std::string>(), "rebin")
    ;
        //("Path,p", po::value<std::string>(), "path of input folder")
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    
    if (vm.count("help") || (vm.count("DATA") == 0) || (vm.count("BKG") == 0) || (vm.count("QCD") == 0) ) {
        std::cout << desc << std::endl;
        return 0;
    }

    std::string inDATA = vm["DATA"].as<std::string>();
    std::string inBKG = vm["BKG"].as<std::string>();
    std::string inQCD = vm["QCD"].as<std::string>();
    std::string inSIG = "" ;
    if ( (vm.count("SIG") == 0) ){
        inSIG = inBKG ;
    } else {
        inSIG = vm["SIG"].as<std::string>();
    }
    

    FitRunner* run = new FitRunner( [](std::string chainName, std::string histName, double LearnRate , double StopEpsilon , double MomentumGamma , std::vector<double> DataNom, std::vector<double> BkgNom, std::vector<double> BkgErr, std::vector<double> SigNom, std::vector<double> SigErr, double* chi, double* factor, double* factorErr){return std::unique_ptr<FitFunction>(new FitFunction(chainName, histName, LearnRate, StopEpsilon, MomentumGamma, DataNom, BkgNom, BkgErr, SigNom, SigErr, chi, factor, factorErr)) ;} );
    
    std::vector<std::string> input = {} ;
    for( int i = 1800 ; i >= 1001 ; i-=10){
        std::string index_i = std::to_string(i);
        input.push_back("m0p0"+index_i.substr(1));
    }
    //input.push_back("0p0000") ;
    for( int i = 1000 ; i<= 1800 ; i+=10){
        std::string index_i = std::to_string(i);
        input.push_back( "0p0"+index_i.substr(1));
    }
    
    std::map<std::string, std::string> hist_name_map;
    //hist_name_map["Path"] = folder_path;
    hist_name_map["DATA"] = inDATA;
    hist_name_map["QCD"] = inQCD;
    hist_name_map["BKG"] = inBKG;
    hist_name_map["SIG"] = inSIG;
    if ( (vm.count("Output") != 0) ){
        std::string outname = vm["Output"].as<std::string>();
        hist_name_map["Out"] = outname ;
    } else {
        hist_name_map["Out"] = "" ;
    }

    input = {""};
    ConfigHolder configs = ConfigHolder(hist_name_map , input);
    
    if ( (vm.count("Rebin") != 0) ){
        std::string Rebin = vm["Rebin"].as<std::string>();
        configs.LoadRebins(Rebin) ;
        std::cout<<configs.checkRebins()<<" : "<<configs.getRebins()<<std::endl;
    }
    if ( (vm.count("JsonPath") != 0) ){
        std::string jPath = vm["JsonPath"].as<std::string>();
        std::ifstream f(jPath);
        json jfile = json::parse(f);
        for ( auto& it : jfile["ActivePeriod"] ){
            configs.addPeriod( it ) ;
        }

        for( std::string periodName : configs.ActivePeriod ){
            for ( auto& it : jfile["Data"][periodName] ){
                configs.addPairsToData( it , periodName ) ;
            }
            for ( auto& it : jfile["QCD"][periodName] ){
                configs.addPairsToQCD( it , periodName) ;
            }
            for ( auto& it : jfile["Signal"][periodName] ){
                configs.addPairsToSIG( it , periodName) ;
            }
            for ( auto& it : jfile["Backgrounds"][periodName] ){
                configs.addPairsToBKG( it , periodName) ;
            }

            configs.k_w_1p_os[periodName] = jfile["k_w_1p_os"][periodName] ;
            configs.k_w_1p_ss[periodName] = jfile["k_w_1p_ss"][periodName] ;
            configs.k_w_3p_os[periodName] = jfile["k_w_3p_os"][periodName] ;
            configs.k_w_3p_ss[periodName] = jfile["k_w_3p_ss"][periodName] ;
            configs.r_1p[periodName]      = jfile["r_1p"][periodName]      ;
            configs.r_3p[periodName]      = jfile["r_3p"][periodName]      ;
        
        }

        configs.LearnRate     = jfile["LearnRate"] ;
        configs.Stop          = jfile["Stop"] ;
        configs.MomentumGamma = jfile["MomentumGamma"] ;
    }
    
    run->runChains(configs);
    //            getAnaCollection()[#ANALYSISNAME] = [] {                                                             
    //                return new SingleTreeRunner([](std::string chainName, std::shared_ptr<AnaOutputHandler> output) {
    //                    return std::unique_ptr<ANALYSISNAME>(new ANALYSISNAME(chainName, output));                   
    //                });                                                                                              
    //            };                                                                                                   
}
